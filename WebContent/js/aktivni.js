
function onLoad(){
	$.ajax({
		url: 'rest/apartman/getMojeApartmaneA',
		type: 'get',
		success: function(apartmani) {
			if(apartmani==null){
				alert('Nema apartmana');
				
			}else {
				ispisiApartmane(apartmani);		
			} 
		}
	});
	
	
	
	$(document).ready(function(){
		
	    $("input#imgSource").change(function(e) {
	        //  alert('changed!');
	          const image = e.target.files[0];
	  		const reader = new FileReader();
	  		reader.readAsDataURL(image);
	  		
	  		reader.onload = e => {
	  			$('img#sl').attr('src',  e.target.result);
	  		}
	  		
	      });
	});
	
	
	$('#sobaSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/brojSoba/asc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	
	$('#sobaSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/brojSoba/desc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	

	$('#gostiSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/brojGostiju/asc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	
	$('#gostiSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/brojGostiju/desc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	


	$('#cijenaSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/cijena/asc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	
	$('#cijenaSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/apartman/cijena/desc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
		
//	$('#tipSortRastuce').on('click',function(){ 
//		$.ajax({
//			type: 'get',
//			url: "rest/apartman/tip/asc",
//			contentType:"application/json",
//
//			success: function(apartmani) {
//				$('#tbApartman tbody').empty();	
//				//for(apartmani){
//					ispisiApartmane(apartmani);
//				//}
//
//			}
//		  })
//
//
//		});
//	
//	$('#tipSortOpadajuce').on('click',function(){ 
//		$.ajax({
//			type: 'get',
//			url: "rest/apartman/tip/desc",
//			contentType:"application/json",
//
//			success: function(apartmani) {
//				$('#tbApartman tbody').empty();	
//				//for(apartmani){
//					ispisiApartmane(apartmani);
//				//}
//
//			}
//		  })
//
//
//		});	
	
}

function ispisiApartmane(apartman){
	var korisnik = sessionStorage.getItem("korisnik");
	var user = JSON.parse(korisnik);
	 var list = apartman == null ? [] : (apartman instanceof Array ? apartman : [ apartman ]);
		
	 $.each(apartman, function(index, o) {
		 //if(user.uloga==="Admininstrator"){
			 ispisiApartmaan(o);
		// }
		
	 });

	
}


//function sortTable(n) {
//	  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
//	  table = document.getElementById("tbApartman");
//	  switching = true;
//	  // Set the sorting direction to ascending:
//	  dir = "asc";
//	  /* Make a loop that will continue until
//	  no switching has been done: */
//	  while (switching) {
//	    // Start by saying: no switching is done:
//	    switching = false;
//	    rows = table.rows;
//	    /* Loop through all table rows (except the
//	    first, which contains table headers): */
//	    for (i = 1; i < (rows.length - 1); i++) {
//	      // Start by saying there should be no switching:
//	      shouldSwitch = false;
//	      /* Get the two elements you want to compare,
//	      one from current row and one from the next: */
//	      x = rows[i].getElementsByTagName("TD")[n];
//	      y = rows[i + 1].getElementsByTagName("TD")[n];
//	      /* Check if the two rows should switch place,
//	      based on the direction, asc or desc: */
//	      if (dir == "asc") {
//	        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
//	          // If so, mark as a switch and break the loop:
//	          shouldSwitch = true;
//	          break;
//	        }
//	      } else if (dir == "desc") {
//	        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
//	          // If so, mark as a switch and break the loop:
//	          shouldSwitch = true;
//	          break;
//	        }
//	      }
//	    }
//	    if (shouldSwitch) {
//	      /* If a switch has been marked, make the switch
//	      and mark that a switch has been done: */
//	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
//	      switching = true;
//	      // Each time a switch is done, increase this count by 1:
//	      switchcount ++;
//	    } else {
//	      /* If no switching has been done AND the direction is "asc",
//	      set the direction to "desc" and run the while loop again. */
//	      if (switchcount == 0 && dir == "asc") {
//	        dir = "desc";
//	        switching = true;
//	      }
//	    }
//	  }
//	}
//



function ispisiApartmaan(o){
	
	let domacin = $('<td>'+o.domacin+'</td>');
	let id = $('<td>'+o.id+'</td>');
	let tip = $('<td>'+o.tip+'</td>');
	let lokacija = $('<td>'+o.lokacija+'</td>');
	let brojSoba = $('<td>'+o.brojSoba+'</td>');
	let brojGostiju = $('<td>'+o.brojGostiju+'</td>');
	let komentari = $('<td>'+o.komentari+'</td>');
	let cijenaPoNoci = $('<td>'+o.cijenaPoNoci+'</td>');
	let datumOd = $('<td>'+o.datumOd+'</td>');
	let datumDo = $('<td>'+o.datumDo+'</td>');
	let status = $('<td>'+o.status+'</td>');
	
	let slika = $('<td>'+o.imgSlika+'</td>');
	let lista = $('<td></td>');
	let rec= o.listaSadrzajaApartmana;
		for(r of rec){
			let li = $('<option>'+r+'</option>');
			lista.append(li);
		}
	
		let tr = $('<tr></tr>');
		tr.append(domacin).append(id).append(tip).append(lokacija).append(brojSoba).append(brojGostiju).append(komentari).append(cijenaPoNoci).append(datumOd).append(datumDo).append(status).append(slika).append(lista);//.append(i);//..append(tdObrisi);//.append(sadrzaji);
		$('table#tbApartman tbody').append(tr);
}

	
	
	function removeAll() {
		$('table#tbApartman tbody tr').remove();
		//$('select#aktApartman option').remove();
	}
	


	