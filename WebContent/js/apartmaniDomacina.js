
function onLoad(){
	$.ajax({
		url: 'rest/apartman/getMojeApartmanee',
		type: 'get',
		success: function(apartmani) {
			if(apartmani==null){
				alert('Nema apartmana');
			}else {
				ispisiApartmane(apartmani);		
			} 
		}
	});
	
	$('#cijenaSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/users/cijena5/asc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});
	
	$('#cijenaSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/users/cijena5/desc",
			contentType:"application/json",

			success: function(apartmani) {
				$('#tbApartman tbody').empty();	
				//for(apartmani){
					ispisiApartmane(apartmani);
				//}

			}
		  })


		});

	
	
	$(document).ready(function(){
		
	    $("input#imgSource").change(function(e) {
	        //  alert('changed!');
	          const image = e.target.files[0];
	  		const reader = new FileReader();
	  		reader.readAsDataURL(image);
	  		
	  		reader.onload = e => {
	  			$('img#sl').attr('src',  e.target.result);
	  		}
	  		
	      });
	});
	
}

function ispisiApartmane(apartman){
	var korisnik = sessionStorage.getItem("korisnik");
	var user = JSON.parse(korisnik);
	 var list = apartman == null ? [] : (apartman instanceof Array ? apartman : [ apartman ]);
		
	 $.each(apartman, function(index, o) {
		 //if(user.uloga==="Admininstrator"){
			 ispisiApartmaan(o);
		// }
		
	 });

	
}

function ispisiApartmaan(o){
	
	let domacin = $('<td>'+o.domacin+'</td>');
	let id = $('<td>'+o.id+'</td>');
	let tip = $('<td>'+o.tip+'</td>');
	let lokacija = $('<td>'+o.lokacija+'</td>');
	let brojSoba = $('<td>'+o.brojSoba+'</td>');
	let brojGostiju = $('<td>'+o.brojGostiju+'</td>');
	let komentari = $('<td>'+o.komentari+'</td>');
	let cijenaPoNoci = $('<td>'+o.cijenaPoNoci+'</td>');
	let datumOd = $('<td>'+o.datumOd+'</td>');
	let datumDo = $('<td>'+o.datumDo+'</td>');
	let status = $('<td>'+o.status+'</td>');
	let i = $('<td><a href="izmijeniApartmane.html">izmjeni</a></td>');
	i.click(function(event){
		 $.get({
				url : 'rest/rezervacija/uzmiAppZaIzmjenu?o='+o.id,
				success : function(data){
					if(data==null){
					}else {
						
						
					
	    		}		
				}
			});
		
	
	});	let slika = $('<td>'+o.imgSlika+'</td>');
	let lista = $('<td></td>');
	let rec= o.listaSadrzajaApartmana;
		for(r of rec){
			let li = $('<option>'+r+'</option>');
			lista.append(li);
		}
	
		let tdObrisi = $('<td></td>');
		if(o.status==="AKTIVAN"){
			let aObrisi = $('<a href="">Obrisi</a>');
			aObrisi.click(function(event){
				event.preventDefault();
				let dd = o.id;
				$.ajax({
					url:'rest/apartman/obrisi/'+dd,
					type:'PUT',
					success: function(){
						$.ajax({
							url : 'rest/users/getMojeApartmanee',
							type:'get',
							success : function(accounts) {
								removeAll();
								for(acc of accounts){
									ispisiApartmaan(acc);
								}
							}
						});
					}
				});
			});
			tdObrisi.append(aObrisi);
		}else{
			let aAktiviraj = $('<a href="">Aktiviraj</a>');
			aAktiviraj.click(function(event){
				event.preventDefault();
				let ii = o.id;
				$.ajax({
					url:'rest/apartman/activate/'+ii,
					type:'PUT',
					success: function(){
						$.get({
							url : 'rest/users/getMojeApartmanee',
							success : function(accounts) {
								removeAll();
								for(acc of accounts){
									ispisiApartmaan(acc);
								}
							}
						});
					}
				});
			});
			tdObrisi.append(aAktiviraj);
		}

		let tr = $('<tr></tr>');
		tr.append(domacin).append(id).append(tip).append(lokacija).append(brojSoba).append(brojGostiju).append(komentari).append(cijenaPoNoci).append(datumOd).append(datumDo).append(status).append(slika).append(lista).append(i).append(tdObrisi);//.append(sadrzaji);
		$('table#tbApartman tbody').append(tr);
//		if( o.status==="AKTIVAN"){
//			let opt = $('<option>'+o.id+'</option>');
//			$('select#aktOglasi').append(opt);
//		}
//		
	}

	
	
	function removeAll() {
		$('table#tbApartman tbody tr').remove();
		//$('select#aktApartman option').remove();
	}
	


	