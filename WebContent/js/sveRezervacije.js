
function onLoad(){
	$.ajax({
		url: 'rest/rezervacija/',
		type: 'get',
		success: function(rezervacije) {
			if(rezervacije==null){
				alert('Nema rezevacija');
			}else {
				ispisiRezevacije(rezervacije);		
			} 
		}
	});
	
	$('#cijenaSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/rezervacija/cijena4/asc",
			contentType:"application/json",

			success: function(rez) {
				$('#tbRez tbody').empty();	
				//for(apartmani){
				ispisiRezevacije(rez);
				//}

			}
		  })


		});
	
	$('#cijenaSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/rezervacija/cijena4/desc",
			contentType:"application/json",

			success: function(rez) {
				$('#tbRez tbody').empty();	
				//for(apartmani){
				ispisiRezevacije(rez);
				//}

			}
		  })


		});

}

function ispisiRezevacije(rezervacije){
	 var list = rezervacije == null ? [] : (rezervacije instanceof Array ? rezervacije : [ rezervacije ]);
		
	 $.each(rezervacije, function(index, o) {
		 	 ispisiRezervacijuu(o);
		
	 });

	
}

function ispisiRezervacijuu(o){
	
	
	let broj_Nocenja = $('<td>'+o.broj_Nocenja+'</td>');
	let cijena = $('<td>'+o.cijena+'</td>');
	
	let poruka = $('<td>'+o.poruka+'</td>');
	let status = $('<td>'+o.status+'</td>');
	let gost=$('<td>'+o.gost.ime+'</td>');
	let apartmanID=$('<td>'+o.apartman.id+'</td>');
	let lokacija=$('<td>'+o.apartman.lokacija+'</td>');
	let brojSoba=$('<td>'+o.apartman.brojSoba+'</td>');
	let komentari=$('<td>'+o.apartman.komentari+'</td>');
	let brojGostiju=$('<td>'+o.apartman.brojGostiju+'</td>');

	
	let tr = $('<tr></tr>');
	tr.append(broj_Nocenja).append(poruka).append(cijena).append(status).append(gost).append(apartmanID).append(lokacija).append(brojSoba).append(brojGostiju).append(komentari);
	$('table#tbRez tbody').append(tr);
//		if( o.status==="AKTIVAN"){
//			let opt = $('<option>'+o.id+'</option>');
//			$('select#aktOglasi').append(opt);
//		}
//		
	}

	
	
	function removeAll() {
		$('table#tbRez tbody tr').remove();
		//$('select#aktApartman option').remove();
	}
	