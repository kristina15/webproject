$(document).on('submit','.logovanje',function(e){
	e.preventDefault();
	var dobro=true;
	
	
	let korisnicko = $('#korisnicko').val();
	let lozinka = $('#lozinka').val();
	
	if(!korisnicko || !lozinka)
	{
		$('#error').text('Sva polja moraju biti popunjena!');
		$('#error').show().delay(3000).fadeOut();
		dobro=false;
		return;
	}
	
	
	$.ajax({
		type : 'POST',
		url : "rest/users/uloguj",
		contentType : 'application/json',
		dataType : "json",
		data:formToJSON(),
		success : function(data) {
			if(data!=null) {
				$('#success').text('Uspjesno.');
				$('#success').show().delay(3000).fadeOut();
				sessionStorage.setItem('ulogovan',JSON.stringify(data));
				window.location.href = 'index.html';
			}else {
				alert("Niste ukucali ispravno korisnicko ime ili lozinku");
				console.log("POST METOD GRESKA!!!!");
				$('#error').text('Neuspjesno!');
				$('#error').show().delay(3000).fadeOut();
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert(errorThrown);
		}
	});	
});

function formToJSON() {
	return JSON.stringify({
		"korisnicko":$('#korisnicko').val(),
		"lozinka":$('#lozinka').val()
	});
}
