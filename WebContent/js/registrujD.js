
$(document).on('submit','.registracija',function(e){
	
		event.preventDefault();
		    var dobro = true;
			let username = $('#username').val();
			let password = $('#password').val();
			let ime = $('#ime').val();
			let prezime = $('#prezime').val();
			let pol = $('#pol').val();
			var password1 =document.getElementById("password");
			var password2=document.getElementById("password1");
			var loz1 = password1.value;
			var loz2 = password2.value;
			document.getElementById("labelaLozinka").innerHTML="";
			

			if(loz1!=loz2){
				password1.style.border = "1px solid red";
				password2.style.border = "1px solid red";
				document.getElementById("labelaLozinka").innerHTML="Lozinke se poklapaju.";
				dobro = false;
				
			}
			 $.get({
					url : 'rest/users/uzmiTrenutnog?o='+username,
					success : function(data){
						if(data==null){
							
						}else {
							
							
						
		    		}		
					}
				});
			
			
			
			
			if(!username || !password || !ime || !prezime  || !pol)
			{
				$('#error').text('Sva polja moraju biti popunjena!');
				$('#error').show().delay(3000).fadeOut();
				return;
			}
			if (dobro==true){
			$.post({
				url: 'rest/users/registracijaD',
				data: JSON.stringify({korisnickoIme:username, lozinka:password,ime:ime, prezime:prezime,pol:pol}),
				contentType: 'application/json',
				success: function(data) {	
					if (data!=null){
						
						console.log("POST METOD OK!!!");
						sessionStorage.setItem('ulogovan',JSON.stringify(data));
						$('#success').text('Korisnik uspešno registrovan.');
						$('#success').show().delay(3000).fadeOut();
					    window.location='index.html';
					}else{
						console.log("POST METOD GRESKA!!!!");
						$('#error').text('Korisnik sa tim korisnickim imenom vec postoji!');
						$('#error').show().delay(3000).fadeOut();
					}
						
				},
				error: function(message) {
					$('#error').text(message);
					$('#error').show().delay(3000).fadeOut();
				}
			});
}});