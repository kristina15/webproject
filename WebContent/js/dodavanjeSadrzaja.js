

$(document).on('submit','.sadrzaj',function(e){
	e.preventDefault();
	$.ajax({
		type : 'POST',
		url : "rest/sadrzaj/dodajSadrzaj",
		contentType : 'application/json',
		dataType : "json",
		data:formToJSON(),
		success : function(data) {
			if(data==null){
				alert("Id nije jednistven!");
				
			}else {
					alert("Sadrzaj uspjesno dodat!");
					window.location.href = "index.html";
					
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			   
		}
	});
});

function formToJSON() {
	return JSON.stringify({
		"naziv" : $('#naziv').val(),
		"id" : $('#id').val(),
	});
}