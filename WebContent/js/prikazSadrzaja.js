function onLoad(){
	$.ajax({
		url: 'rest/sadrzaj/',
		type: 'get',
		success: function(sadrzaj) {
			if(sadrzaj==null){
			}else {
				ispisiSadrzaj(sadrzaj);
				console.log("ispisuje");

			} 
		}
		
	});
}

$(document).ready(function(){
	$('button#sacuvaj').click(function(){
		let naziv = $('input[name=naziv]').val();
		//let id = $('input[name=id]').val();
		let stariNaziv = $('select#aktSad').val();
		let flag = true;
		if(naziv==null || naziv===""){
			let td=$('<td>Popunite ovo polje!</td>');
			let tr = $($('table#izmjena tbody tr')[1]);
			tr.append(td);
			flag = false;
		}
		
		let url = 'rest/sadrzaj/izmjeni/' +stariNaziv+','+naziv;
		console.log(url);
		if(flag){
			$.ajax({
				url:url,
				type: 'PUT',
				//data:JSON.stringify({stariNaziv:stariNaziv,naziv:naziv}),
				success: function(){
					$.get({
						url : 'rest/sadrzaj',
						success : function(accounts) {
						//	$('select#aktSad').remove();
							removeAll();
							for(acc of accounts){
								ispisiSad(acc);
							}
						}
					});
				}
			});
		}
		
	});
	
		
	});


 

function ispisiSadrzaj(sadrzaj){
	 var list = sadrzaj == null ? [] : (sadrzaj instanceof Array ? sadrzaj : [ sadrzaj ]);
		console.log("ispisi sadrzaj");
	 $.each(sadrzaj, function(index, o) {
	 ispisiSad(o);	 
	 });

	
}

function ispisiSad(o){
	
	let naziv = $('<td>'+o.naziv+'</td>');
	let id = $('<td>'+o.id+'</td>');
	let aktivan = $('<td>'+o.ak+'</td>');
	let tdObrisi = $('<td></td>');
	if(o.ak==="Da"){
		let aObrisi = $('<a href="">Obrisi</a>');
		aObrisi.click(function(event){
			event.preventDefault();
			let pp = o.naziv;
			console.log(pp);
			$.ajax({
				url:'rest/sadrzaj/obrisi/'+pp,
				type:'PUT',
				success: function(sad){
					
					$.ajax({
						url : 'rest/sadrzaj',
						type:'get',
						success : function(accounts) {
							//alert("Sadrzaj uspjesno dodat!");
							
							removeAll();
							for(acc of accounts){
								ispisiSad(acc);
							}
						}
					});
				}
			});
		});
		tdObrisi.append(aObrisi);
	
	}else{
		let aAktiviraj = $('<a href="">Aktiviraj</a>');
		aAktiviraj.click(function(event){
			event.preventDefault();
			let pp = o.naziv;
			$.ajax({
				url:'rest/sadrzaj/activate/'+pp,
				type:'PUT',
				success: function(k){
					console.log(k);
					$.get({
						url : 'rest/sadrzaj',
						success : function(accounts) {
							removeAll();
							for(acc of accounts){
								ispisiSad(acc);
							}
						}
					});
				}
			});
		});
		tdObrisi.append(aAktiviraj);
	}
	let tr = $('<tr></tr>');
	tr.append(naziv).append(id).append(aktivan).append(tdObrisi);
	$('table#tbSadrzaj tbody').append(tr);
	if(o.aktivan === true){
		let opt = $('<option>'+o.naziv+'</option>');
		$('select#aktSad').append(opt);
	}
}

function removeAll() {
	$('table#tbSadrzaj tbody tr').remove();
	$('select#aktSad option').remove();
}