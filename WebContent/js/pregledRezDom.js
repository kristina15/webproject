
function onLoad(){
	$.ajax({
		url: 'rest/rezervacija/rezDomacina',
		type: 'get',
		success: function(rezervacije) {
			if(rezervacije==null){
				alert('Nema rezevacija');
			}else {
				ispisiRezevacije(rezervacije);		
			} 
		}
	});
	
	}

function ispisiRezevacije(rezervacije){
	 var list = rezervacije == null ? [] : (rezervacije instanceof Array ? rezervacije : [ rezervacije ]);
		
	 $.each(rezervacije, function(index, o) {
		 	 ispisiRezervacijuu(o);
		
	 });

	
}

function ispisiRezervacijuu(o){
	
	
	let broj_Nocenja = $('<td>'+o.broj_Nocenja+'</td>');
	let cijena = $('<td>'+o.cijena+'</td>');
	
	let poruka = $('<td>'+o.poruka+'</td>');
	let status = $('<td>'+o.status+'</td>');
	let gost=$('<td>'+o.gost.ime+'</td>');
	let apartmanID=$('<td>'+o.apartman.id+'</td>');
	let lokacija=$('<td>'+o.apartman.lokacija+'</td>');
	let brojSoba=$('<td>'+o.apartman.brojSoba+'</td>');
	let komentari=$('<td>'+o.apartman.komentari+'</td>');
	
	let tdOdbij = $('<td></td>');
	if(o.status==="KREIRANA" || o.status==="PRIHVACENA"){
		let aOdbij = $('<a href="">Odbij</a>');
		aOdbij.click(function(event){
			event.preventDefault();
			let ii = o.id;
			$.ajax({
				url:'rest/rezervacija/obrisiRez/'+ii,
				type:'PUT',
				success: function(){
					$.ajax({
						url : 'rest/rezervacija/rezDomacina',
						type:'get',
						success : function(accounts) {
							removeAll();
							ispisiRezevacije(accounts);
						}
					});
				}
			});
		});
		tdOdbij.append(aOdbij);
	}
	
	
	//PRIHVATI REZERVACIJU
	let tdPrihvati = $('<td></td>');
	if(o.status==="KREIRANA"){
		let aPrihvati = $('<a href="">Prihvati</a>');
		aPrihvati.click(function(event){
			event.preventDefault();
			let pp = o.id;
			$.ajax({
				url:'rest/rezervacija/prihvati/'+pp,
				type:'PUT',
				success: function(){
					$.ajax({
						url : 'rest/rezervacija/rezDomacina',
						type:'get',
						success : function(accounts) {
							removeAll();
							ispisiRezevacije(accounts);
						}
					});
				}
			});
		});
		tdPrihvati.append(aPrihvati);
	}
	
	
	
	
	
	
	let tr = $('<tr></tr>');
	tr.append(broj_Nocenja).append(poruka).append(cijena).append(status).append(gost).append(apartmanID).append(lokacija).append(brojSoba).append(komentari).append(tdPrihvati).append(tdOdbij);//.append(tdOdustani);
	$('table#tbRez tbody').append(tr);
//		if( o.status==="AKTIVAN"){
//			let opt = $('<option>'+o.id+'</option>');
//			$('select#aktOglasi').append(opt);
//		}
//		
	}

	
	
	function removeAll() {
		$('table#tbRez tbody tr').remove();
		//$('select#aktApartman option').remove();
	}
	