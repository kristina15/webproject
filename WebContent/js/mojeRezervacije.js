
function onLoad(){
	$.ajax({
		url: 'rest/rezervacija/getMojeRezervacije',
		type: 'get',
		success: function(rezervacije) {
			if(rezervacije==null){
				alert('Nema rezevacija');
			}else {
				ispisiRezevacije(rezervacije);		
			} 
		}
	});
	
	
	
	$('#cijenaSortRastuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/rezervacija/cijena/asc",
			contentType:"application/json",

			success: function(rez) {
				$('#tbRez tbody').empty();	
				//for(apartmani){
				ispisiRezevacije(rez);
				//}

			}
		  })


		});
	
	$('#cijenaSortOpadajuce').on('click',function(){ 
		$.ajax({
			type: 'get',
			url: "rest/rezervacija/cijena/desc",
			contentType:"application/json",

			success: function(rez) {
				$('#tbRez tbody').empty();	
				//for(apartmani){
				ispisiRezevacije(rez);
				//}

			}
		  })


		});

}

function ispisiRezevacije(rezervacije){
	 var list = rezervacije == null ? [] : (rezervacije instanceof Array ? rezervacije : [ rezervacije ]);
		
	 $.each(rezervacije, function(index, o) {
		 	 ispisiRezervacijuu(o);
		
	 });

	
}

function ispisiRezervacijuu(o){
	
	
	let broj_Nocenja = $('<td>'+o.broj_Nocenja+'</td>');
	let cijena = $('<td>'+o.cijena+'</td>');
	
	let poruka = $('<td>'+o.poruka+'</td>');
	let status = $('<td>'+o.status+'</td>');
	let gost=$('<td>'+o.gost.ime+'</td>');
	let apartmanID=$('<td>'+o.apartman.id+'</td>');
	let lokacija=$('<td>'+o.apartman.lokacija+'</td>');
	let brojSoba=$('<td>'+o.apartman.brojSoba+'</td>');
	
    //let komentar = $('<td><a href="ostaviKomentar.html">Ostavi komentar</a></td>');
	
	
	
//	komentar.click(function(event){
//		 $.get({
//				url : 'rest/rezervacija/uzmiApp?o='+o.id,
//				success : function(data){
//					if(data==null){
//					}else {
//						
//					
//	    		}		
//				}
//			});
//		
//		
//	});
//	
	
	let tdOdustani = $('<td></td>');
	if(o.status==="KREIRANA" || o.status==="PRIHVACENA"){
		let aOdustani = $('<a href="">Odustani</a>');
		aOdustani.click(function(event){
		event.preventDefault();
		let oo= o.id;
				$.ajax({
					url:'rest/rezervacija/odustani/'+oo,
					type:'PUT',
					success: function(){
						$.ajax({
							url : 'rest/rezervacija/getMojeRezervacije',
							type:'get',
							success : function(accounts) {
								removeAll();
								for(acc of accounts){
									ispisiRezervacijuu(acc);
								}
							}
						});
					}
				});
		});
		tdOdustani.append(aOdustani);
	}else{
		let aAktiviraj = $('<a href="">Ponovo kreiraj</a>');
		aAktiviraj.click(function(event){
			event.preventDefault();
			let pp = o.id;
			$.ajax({
				url:'rest/rezervacija/kreiraj/'+pp,
				type:'PUT',
				success: function(){
					$.get({
						url : 'rest/rezervacija/getMojeRezervacije',
						success : function(accounts) {
							removeAll();
							for(acc of accounts){
								ispisiRezervacijuu(acc);
							}
						}
					});
				}
			});
		});
		tdOdustani.append(aAktiviraj);
	}
	
	let tr = $('<tr></tr>');
	tr.append(broj_Nocenja).append(poruka).append(cijena).append(status).append(gost).append(apartmanID).append(lokacija).append(brojSoba).append(tdOdustani);//.append(komentar);
	$('table#tbRez tbody').append(tr);
//		if( o.status==="AKTIVAN"){
//			let opt = $('<option>'+o.id+'</option>');
//			$('select#aktOglasi').append(opt);
//		}
//		
	}

	
	
	function removeAll() {
		$('table#tbRez tbody tr').remove();
		//$('select#aktApartman option').remove();
	}
	