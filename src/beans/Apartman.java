package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

enum Status{
	AKTIVNO,NEAKTIVNO
}

public class Apartman {
	private String tip;
	private String ime;
	private String id;
	private Korisnik gost;
	
	public Korisnik getGost() {
		return gost;
	}
	public void setGost(Korisnik gost) {
		this.gost = gost;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	private int brojSoba;
	private int brojGostiju;
	private String lokacija;
	private ArrayList<String> dostupniDatumi = new ArrayList<>();
	public ArrayList<String> getDostupniDatumi() {
		return dostupniDatumi;
	}
	public void setDostupniDatumi(ArrayList<String> dostupniDatumi) {
		this.dostupniDatumi = dostupniDatumi;
	}
	private String domacin;
	private String komentari;//Komentari za apartman koje daju gosti koji su posetili apartman
    private String imgSlika;
    private double cijenaPoNoci;
    private String vrijemeZaPrijavu;
    private String vrijemeZaOdjavu;
    private String status;
    private ArrayList<String> listaSadrzajaApartmana = new ArrayList<>();
    private ArrayList<Integer> listaRezervacija = new ArrayList<>();
    private String name;

	private String datumOd;
	private String datumDo;
	
	
	
	private String namee;
	
	public Apartman(String namee) {
		this.namee=namee;
	}
	
	
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getDatumOd() {
		return datumOd;
	}
	public void setDatumOd(String datumOd) {
		this.datumOd = datumOd;
	}
	public String getDatumDo() {
		return datumDo;
	}
	public void setDatumDo(String datumDo) {
		this.datumDo = datumDo;
	}
	public int getBrojSoba() {
		return brojSoba;
	}
	public void setBrojSoba(int brojSoba) {
		this.brojSoba = brojSoba;
	}
	public int getBrojGostiju() {
		return brojGostiju;
	}
	public void setBrojGostiju(int brojGostiju) {
		this.brojGostiju = brojGostiju;
	}
	public String getLokacija() {
		return lokacija;
	}
	public void setLokacija(String lokacija) {
		this.lokacija = lokacija;
	}
	
	public String getDomacin() {
		return domacin;
	}
	public void setDomacin(String domacin) {
		this.domacin = domacin;
	}
	public String getKomentari() {
		return komentari;
	}
	public void setKomentari(String komentari) {
		this.komentari = komentari;
	}
	
	
	
	public String getImgSlika() {
		return imgSlika;
	}
	public void setImgSlika(String imgSlika) {
		this.imgSlika = imgSlika;
	}

	public String getVrijemeZaPrijavu() {
		return vrijemeZaPrijavu;
	}
	public void setVrijemeZaPrijavu(String vrijemeZaPrijavu) {
		this.vrijemeZaPrijavu = vrijemeZaPrijavu;
	}
	public String getVrijemeZaOdjavu() {
		return vrijemeZaOdjavu;
	}
	public void setVrijemeZaOdjavu(String vrijemeZaOdjavu) {
		this.vrijemeZaOdjavu = vrijemeZaOdjavu;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public ArrayList<String> getListaSadrzajaApartmana() {
		return listaSadrzajaApartmana;
	}
	public void setListaSadrzajaApartmana(ArrayList<String> listaSadrzajaApartmana) {
		this.listaSadrzajaApartmana = listaSadrzajaApartmana;
	}
	public ArrayList<Integer> getListaRezervacija() {
		return listaRezervacija;
	}
	public void setListaRezervacija(ArrayList<Integer> listaRezervacija) {
		this.listaRezervacija = listaRezervacija;
	}
	

	public Apartman(String tip, String ime, String id, int brojSoba, int brojGostiju, String lokacija,
			ArrayList<String> dostupniDatumi, String domacin, String komentari, String imgSlika, double cijenaPoNoci,
			String vrijemeZaPrijavu, String vrijemeZaOdjavu, String status, ArrayList<String> listaSadrzajaApartmana,
			ArrayList<Integer> listaRezervacija, String datumOd, String datumDo,Korisnik gost) {
		super();
		this.tip = tip;
		this.ime = ime;
		this.id = id;
		this.brojSoba = brojSoba;
		this.brojGostiju = brojGostiju;
		this.lokacija = lokacija;
		this.dostupniDatumi = dostupniDatumi;
		this.domacin = domacin;
		this.komentari = komentari;
		this.imgSlika = imgSlika;
		this.cijenaPoNoci = cijenaPoNoci;
		this.vrijemeZaPrijavu = vrijemeZaPrijavu;
		this.vrijemeZaOdjavu = vrijemeZaOdjavu;
		this.status = status;
		this.listaSadrzajaApartmana = listaSadrzajaApartmana;
		this.listaRezervacija = listaRezervacija;
		this.datumOd = datumOd;
		this.datumDo = datumDo;
		this.gost=gost;
	}
	public double getCijenaPoNoci() {
		return cijenaPoNoci;
	}
	public void setCijenaPoNoci(double cijenaPoNoci) {
		this.cijenaPoNoci = cijenaPoNoci;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNamee() {
		return namee;
	}
	public void setNamee(String namee) {
		this.namee = namee;
	}
	public Apartman() {
		super();
		this.vrijemeZaPrijavu = "2 PM";
		this.vrijemeZaOdjavu = "10 AM";
		this.status = "NEAKTIVAN";
		this.listaRezervacija=new ArrayList<Integer>();
		this.listaSadrzajaApartmana=new ArrayList<String>();
		// TODO Auto-generated constructor stub
	}
    
	
    
}
