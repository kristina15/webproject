package beans;

import java.util.ArrayList;
import java.util.HashMap;

public class Korisnik {

	private String korisnickoIme;
	private String lozinka;
	private String ime;
	private String prezime;
	private String pol;
	private String uloga;
	
	
	private String namee;
	
	public Korisnik(String namee) {
		this.namee=namee;
	}
	
	
	//gost
	
	 private ArrayList<String> IznajmljeniApartmani = new ArrayList<String>();
	 private ArrayList<Integer> ListaRezervacija = new ArrayList<>();
	 
	 //domacin
	 
	 private ArrayList<String> ApartmaniZaIzdavanje = new ArrayList<String>();
	
	public ArrayList<String> getIznajmljeniApartmani() {
		return IznajmljeniApartmani;
	}
	public void setIznajmljeniApartmani(ArrayList<String> iznajmljeniApartmani) {
		IznajmljeniApartmani = iznajmljeniApartmani;
	}
	public ArrayList<Integer> getListaRezervacija() {
		return ListaRezervacija;
	}
	public void setListaRezervacija(ArrayList<Integer> listaRezervacija) {
		ListaRezervacija = listaRezervacija;
	}
	public ArrayList<String> getApartmaniZaIzdavanje() {
		return ApartmaniZaIzdavanje;
	}
	public void setApartmaniZaIzdavanje(ArrayList<String> apartmaniZaIzdavanje) {
		ApartmaniZaIzdavanje = apartmaniZaIzdavanje;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme; 
	}
	public	 String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getUloga() {
		return uloga;
	}
	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	public Korisnik(String korisnickoIme, String lozinka, String ime, String prezime, String pol, String uloga) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.pol = pol;
		this.uloga = uloga;
	}
	public Korisnik() {
	}
	
	
}
