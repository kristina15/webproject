package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Razervacija {
        Apartman apartman;
        Date pocetak;
        int broj_Nocenja;
        double cijena;
		String poruka;
        Korisnik gost;
        String status;
        int id;
        
        private String name;
        
    	public Razervacija(String name) {
    		this.name=name;
    	}
        
        
        public double getCijena() {
			return cijena;
		}
		public void setCijena(double cijena) {
			this.cijena = cijena;
		}
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
       
        
		public Razervacija(Apartman apartman, Date pocetak, int broj_Nocenja, double cijena, String poruka, Korisnik gost,
				String status, int id) {
			super();
			this.apartman = apartman;
			this.pocetak = pocetak;
			this.broj_Nocenja = broj_Nocenja;
			this.cijena = cijena;
			this.poruka = poruka;
			this.gost = gost;
			this.status = status;
			this.id = id;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public Apartman getApartman() {
			return apartman;
		}
		public void setApartman(Apartman apartman) {
			this.apartman = apartman;
		}
		public Date getPocetak() {
			return pocetak;
		}
		public void setPocetak(Date pocetak) {
			this.pocetak = pocetak;
		}
		public int getBroj_Nocenja() {
			return broj_Nocenja;
		}
		public void setBroj_Nocenja(int broj_Nocenja) {
			this.broj_Nocenja = broj_Nocenja;
		}

		public String getPoruka() {
			return poruka;
		}
		public void setPoruka(String poruka) {
			this.poruka = poruka;
		}
		public Korisnik getGost() {
			return gost;
		}
		public void setGost(Korisnik gost) {
			this.gost = gost;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public Razervacija() {
			super();
			// TODO Auto-generated constructor stub
		}
	
		
	
        
        
        
}
