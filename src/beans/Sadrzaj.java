package beans;

public class Sadrzaj {
     private String id;
     private String naziv;//npr. parking, kuhinja, pegla, ve� ma�ina, itd.
     private String stariNaziv;
     private String name;
     
     
     public Sadrzaj(String name) {
    	 this.name=name;
    	 
     }
     
     public String getStariNaziv() {
		return stariNaziv;
	}
	public void setStariNaziv(String stariNaziv) {
		this.stariNaziv = stariNaziv;
	}
	private boolean aktivan;
 	 private String ak;
 	 
	public boolean isAktivan() {
		return aktivan;
	}
	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	
	
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	
	
	
	public Sadrzaj(String id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
		aktivan = true;
		this.ak="Da";
	}
	public Sadrzaj() {
		super();
		ak="Da";
		this.aktivan = true;

		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
     
     
}
