package services;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Apartman;
import beans.Korisnik;
import beans.Logovanje;
import beans.Razervacija;
import beans.Sadrzaj;
import dao.ApartmanDAO;
import dao.RezervacijaDAO;
import dao.SadrzajDAO;
import dao.UserDAO;

@Path("/rezervacija")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class RezervacijaService {
	
	@Context
	ServletContext ctx;
	private RezervacijaDAO rezDao = new RezervacijaDAO();
	private ApartmanDAO apDao = new ApartmanDAO();
	private UserDAO korDao=new UserDAO();
	
	private String name;
	
	SimpleDateFormat formatter = new SimpleDateFormat("E, MMM dd yyyy");
	
	
	public RezervacijaService() {
		
	}
	
	public RezervacijaService(String name) {
		this.name=name;
	}
	
	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("rezDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			rezDao = new RezervacijaDAO(contextPath);
	    	System.out.println(contextPath+"ovo je putanja");
			this.ctx.setAttribute("rezDAO", rezDao);
		}
		
		if (this.ctx.getAttribute("apDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			apDao = new ApartmanDAO(contextPath);
	    	System.out.println(contextPath+"ovo je putanja");
			this.ctx.setAttribute("apDAO", apDao);
		}
		
		if (this.ctx.getAttribute("korDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
			korDao = new UserDAO(contextPath);
	    	System.out.println(contextPath+"ovo je putanja");
			this.ctx.setAttribute("korDAO", korDao);
		}
	}
	
	@GET
	@Path("/rezDomacina")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Razervacija> getMojeApp(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
		RezervacijaDAO rezDao = (RezervacijaDAO)this.ctx.getAttribute("rezDAO");
		ArrayList<Razervacija> rezList = new ArrayList<>();
		
		if(apDao.getApartmani() == null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				adList.add(ad);
			}
	  	 }
	   }
		for(Apartman a:adList) {
			for(Integer r:a.getListaRezervacija()) {
				rezList.add(rezDao.find(r));
			}
		}
		return rezList;	
	}
	
	
	@PUT
	@Path("/obrisiRez/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Razervacija del(@PathParam("name") int id, @Context HttpServletRequest req) throws NullPointerException{
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		Razervacija k = rezDao.findById(id);
		System.out.println(id);
		if(k!=null) {
		 k.setStatus("ODBIJENA");
         System.out.println("odbijeno");
         //System.out.println(k.getId());
         
		}else {
			System.out.println("ne ");
			//System.out.println(k.getId());
			
		}
	
		//ctx.setAttribute("sadDAO", sadDAO);
		rezDao.saveRezervacija();
		return k;
		//return Response.status(200).build();
		
	}
	
	@PUT
	@Path("/prihvati/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Razervacija prihvati(@PathParam("name") int id, @Context HttpServletRequest req) throws NullPointerException{
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		Razervacija k = rezDao.findById(id);
		System.out.println(id);
		if(k!=null) {
		 k.setStatus("PRIHVACENA");
         System.out.println("prihvacena");
         //System.out.println(k.getId());
         
		}else {
			System.out.println("ne ");
			//System.out.println(k.getId());
			
		}
	
		rezDao.saveRezervacija();
		return k;
		//return Response.status(200).build();
		
	}
	
	
	@GET
	@Produces({ "application/json" })
	public ArrayList<Razervacija> getKorisnike() {
		rezDao = (RezervacijaDAO)ctx.getAttribute("rezDAO");

		ArrayList<Razervacija> sviKor = new ArrayList<Razervacija>();
		
		for (Razervacija k : rezDao.getRezervacije().values()) {
				sviKor.add(k);
			}
		
		
		return sviKor;
	
	}
	
	
	
	@POST
	@Path("/dodajRezervaciju")	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response promjeni(Razervacija rezervacija,@Context HttpServletRequest request)throws NullPointerException, ParseException {
		String contextPath = ctx.getRealPath(""); 
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		UserDAO us=(UserDAO) ctx.getAttribute("korDAO");
		ApartmanDAO apDao=(ApartmanDAO)ctx.getAttribute("apDAO");
		System.out.println("Usao u dodaj rezervaciju");
		Apartman api=(Apartman)request.getSession().getAttribute("apartman");

		
		//Id
		
		int max=0;
		
		for(Razervacija st: rezDao.getRezervacije().values() ) {
			int id=st.getId();
			if(id>max) {
				max=id;
			}
			
		}
		int id1=Integer.valueOf(++max);
		double konCijena=rezervacija.getBroj_Nocenja()*api.getCijenaPoNoci();
		
//		rezervacija.setCijena(konCijena);
//		rezDao.saveRezervacija();
		System.out.println("Konacna cijena je "+konCijena);
		//System.out.println("cijena app je "+rezervacija.getApartman().getCijenaPoNoci());
		
		boolean postoji=false;
		Date datum=rezervacija.getPocetak();
		ArrayList<Date>datumi=new ArrayList<>();
		datumi.add(datum);
		long timeadj=24*60*60*1000;
		Date newDate;
		
		
		Date date = new Date();  
	    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
	    String strDate = formatter.format(date);  
	    System.out.println("Date Format with MM/dd/yyyy : "+strDate);  
		
		//nova lista stringova datuma
	    ArrayList<String> noviDatumi=new ArrayList<>();
		
		for(int i=0;i<rezervacija.getBroj_Nocenja();i++) {
			newDate=new Date(datum.getTime()+timeadj);
			datumi.add(newDate);//datumi koje zeli da rezervise
			datum=newDate;
			System.out.println("novi datum "+datum);
			System.out.println("pretvaranje datuma");
			String d1=formatter.format(datum);
			String dd=d1.replaceAll("\\/","-");
			String[] dijelovi = dd.split("-");
			String prvi=dijelovi[0];
			String drugi=dijelovi[1];
			String treci=dijelovi[2];
			System.out.println("*******");
			String konacni=treci+"-"+prvi+"-"+drugi;

			System.out.println("*******");
			noviDatumi.add(konacni);
			System.out.println(konacni);
		}
		
		
		
		ArrayList<Date>dostupniDatumi=new ArrayList<>();
		
		System.out.println("ppppp"+api+"ppppp");
		//Apartman aaa=apDao.find(api.getId());
		//Apartman a=rezervacija.getApartman();
		/*dostupniDatumi2=a.getDostupniDatumi();//nista nema
		System.out.println("istaaa"+rezervacija.getBroj_Nocenja()+" ****" +rezervacija.getCijena()+"*****"+rezervacija.getStatus()+"*****"+rezervacija.getPoruka()+"**"+rezervacija.getApartman());
		System.out.println("dostpni datumi su:"+dostupniDatumi2);*/
//		for(String s:aaa.getDostupniDatumi()) {
//			System.out.println("----------------"+s);
//		}
//		
		
		boolean postoji1=false;
		System.out.println("Podaci iz api "+api.getCijenaPoNoci());
		for(String s:noviDatumi) {
			postoji1=false;
			System.out.println(s);
			for(String n:api.getDostupniDatumi()) {
				if(n.equals(s)) {
					postoji1=true;
					
					
				}
			}
			if(!postoji1) {
				System.out.println("datum nije slobodan");
				return Response.status(400).build();
			}
		}
		
		//Razervacija r=new Razervacija(api,datum,);
		
		//new SimpleDateFormat("dd/MM/yyyy").parse(sDate1)
		
//		for(Date d:datumi) {
//			postoji=false;
//			for(Date dat:dostupniDatumi) {
//				if(d.equals(dat)) {
//					postoji=true;
//				}
//				
//			}
//			if(!postoji) {
//				System.out.println("datum nije slobodan");
//				return Response.status(400).build();
//			}
//		}
//		System.out.println("datumi svi"+datumi);
		
		Razervacija ret=rezDao.dodajRez(id1,rezervacija.getPocetak(),rezervacija.getBroj_Nocenja(),api,"KREIRANA",konCijena,rezervacija.getPoruka(),rezervacija.getGost());
		///////////*****************
//		for(String s: ret.getApartman().getDostupniDatumi()) {
//			System.out.println("Novi datum jeee: "+s);
//			
//		}
		if(ret==null) {
			return null;
		}else {

//			Apartman app=rezervacija.getApartman();
			Apartman ap=apDao.find(api.getId());
//			apDao.brisanjeDatuma(ap,datumi);
         	apDao.brisanjeDatuma(api, noviDatumi);
         	apDao.brisanjeDatuma(ap, noviDatumi);
    		//Korisnik kor=(Korisnik)request.getSession().getAttribute("trenutni");
    		//Korisnik k=us.findUserByUsername(kor.getKorisnickoIme());
    		//System.out.println("Korisnik kor je "+kor);
            Korisnik k=ret.getGost();
            ap.setGost(k);
            us.saveUsers();
            
            apDao.saveApartman();
            System.out.println("ooooooo   kor ime jeee" +k.getKorisnickoIme());
         
            System.out.println("Korisnik je "+k);
    		//Korisnik kkk=us.findUserByUsername(kor.getKorisnickoIme());
            
            
            //DODAJ IZNAJMLJNE U Gosta I Apartman
           Korisnik o=us.findUserByUsername(k.getKorisnickoIme());
           ArrayList<String> iznajmljeni=o.getIznajmljeniApartmani();
			iznajmljeni.add(api.getId());
			o.setIznajmljeniApartmani(iznajmljeni);
			us.saveUsers();
			apDao.saveApartman();
         
           	
	        ArrayList<Integer> rezervacije=o.getListaRezervacija();
    		rezervacije.add(id1);
    		o.setListaRezervacija(rezervacije);
    		us.saveUsers();
            
			dodajRezervaciju(ap,ret,k);
//			Korisnik korisnik=ret.getGost();
//			Korisnik korisnikk=us.findUserByUsername(korisnik.getKorisnickoIme());
		//	dodajIznajmljeniApartman(api.getId(),k,ret.getId());
			changeRezervaciju(ret,k);
			dodajAp(ap.getId(),k);
			
			
			System.out.println("kreirana rezervacija");
			
		}
		return Response.status(200).build();
		
	}
	
	public void dodajIznajmljeniApartman(String idAp,Korisnik user,int id){

		
		
		

//		ArrayList<String> apZaIzdavanje=k.getApartmaniZaIzdavanje();
//		apZaIzdavanje.add(res.getId());
//		k.setApartmaniZaIzdavanje(apZaIzdavanje);
//		userDao.saveUsers();
//		apDao.saveApartman();
//		apDao.save(contextPath, res);
//	   ctx.setAttribute("apDAO", apDao);

		
		
		
		
		
		UserDAO us=(UserDAO) ctx.getAttribute("korDAO");//gost
		ApartmanDAO apDao=(ApartmanDAO) ctx.getAttribute("apDAO");
		RezervacijaDAO rezDao=(RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		//Korisnik korisnik=us.findUserByUsername(user.getKorisnickoIme());
	//	System.out.println("blejaa"+korisnik);
		ArrayList<String>apartmani=user.getIznajmljeniApartmani();
		apartmani.add(idAp);
		//korisnik.setIznajmljeniApartmani(apartmani);
		user.setIznajmljeniApartmani(apartmani);
        us.saveUsers();	
        apDao.saveApartman();
		
//        ArrayList<Integer>rezervacije=user.getListaRezervacija();
//		rezervacije.add(id);//id rez rezervisanog apartmana
//		System.out.println("korsinik u izmanjje ap"+user);
//		user.setListaRezervacija(rezervacije);
//   		
	}
	
    public void changeRezervaciju(Razervacija r,Korisnik k) {
		String contextPath = ctx.getRealPath(""); 
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		UserDAO us=(UserDAO) ctx.getAttribute("korDAO");
		
    	r.setGost(k);
    	us.saveUsers();
        rezDao.saveRezervacija();
    }
    public void dodajAp(String id,Korisnik k) {
		String contextPath = ctx.getRealPath(""); 
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
		UserDAO us=(UserDAO) ctx.getAttribute("korDAO");
    	Apartman a=apDao.find(id);
    	a.setGost(k);
    	apDao.saveApartman();
        us.saveUsers();
    }
	
	@GET
	@Path("/uzmiApp")
	@Produces({ "application/json" })
	public Apartman logout(@QueryParam("o") String o,@Context HttpServletRequest request) {
		System.out.println("Usao u metodu uzmiApp ");

		ApartmanDAO ap=(ApartmanDAO) ctx.getAttribute("apDAO");
		Apartman a=ap.find(o);

		request.getSession().setAttribute("apartman",a);

		System.out.println("Usao u metodu uzmiApp id apartmana je "+ o);
		
		System.out.println("Usao u metodu d uzme apartman");
		Apartman api=(Apartman)request.getSession().getAttribute("apartman");

		System.out.println("Preuzeti metodi uzmiApp je "+api);
		return api;
	
	}

	
	
	@GET
	@Path("/uzmiAppZaIzmjenu")
	@Produces({ "application/json" })
	public Apartman logoutt(@QueryParam("o") String o,@Context HttpServletRequest request) {
		System.out.println("Usao u metodu uzmiApp ");

		ApartmanDAO ap=(ApartmanDAO) ctx.getAttribute("apDAO");
		Apartman a=ap.find(o);

		request.getSession().setAttribute("zaIzmjenu",a);

		System.out.println("Usao u metodu uzmiApp id apartmana je "+ o);
		
		System.out.println("Usao u metodu d uzme apartman");
		Apartman api=(Apartman)request.getSession().getAttribute("zaIzmjenu");

		System.out.println("Preuzeti metodi uzmiApp je "+api);
		return api;
	
	}
	

	public void dodajRezervaciju(Apartman a,  Razervacija rez,Korisnik k) {
		String contextPath = ctx.getRealPath(""); 
		ApartmanDAO apDao=(ApartmanDAO)ctx.getAttribute("apDAO");
		RezervacijaDAO rezDao=(RezervacijaDAO)ctx.getAttribute("rezDAO");
		UserDAO kor=(UserDAO)ctx.getAttribute("korDAO");
		
		//dodaj rezervaciju na apartman,id rezervacije
		ArrayList<Integer> listaRez=a.getListaRezervacija();
		ArrayList<Integer> listaRezz=k.getListaRezervacija();
		
		listaRez.add(rez.getId());
		//rezDao.savee(rez);
		rezDao.saveRezervacija();
		a.setListaRezervacija(listaRez);
		apDao.saveApartman();
		listaRezz.add(rez.getId());
		k.setListaRezervacija(listaRez); 		
		kor.saveUsers();
		
	}
	
	
	
	@PUT
	@Path("/odustani/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delCat(@PathParam("name") int name, @Context HttpServletRequest req) throws NullPointerException{
//		if (name.contains("%20"))
//    		name.replace("%20", " ");
		RezervacijaDAO rezdaao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		Razervacija r = rezdaao.find(name);
		System.out.println("Odustanak od rez:");
		System.out.println(name);
		if(r!=null) {
		 r.setStatus("ODUSTANAK");
         System.out.println("odustao");
		}else {
			System.out.println("ipak ne");
			
		}
		rezdaao.saveRezervacija();
		return Response.status(200).build();
		
	}

	
	
	@PUT
	@Path("/kreiraj/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activateAcc(@PathParam("id") int id, @Context HttpServletRequest request) {
		
		RezervacijaDAO rezdaao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		Razervacija r = rezdaao.find(id);
		System.out.println("kreiranje rez:");
		System.out.println(id);
		r.setStatus("KREIRANA");
		rezdaao.saveRezervacija();
		return Response.status(200).build();
	}
	
	
	
	@GET
	@Path("/getMojeRezervacije")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Razervacija> getMojeAp(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");//gost

		ArrayList<Razervacija> adList = new ArrayList<>();
		
		RezervacijaDAO apDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		if(user.getListaRezervacija() == null){
			return null;
		}
		
		for(Integer ad : user.getListaRezervacija()){
		        Razervacija r=apDao.find(ad);
		        adList.add(r);
					
				
			}
		
		
		return adList;	
	}

	@GET
	@Path("/cijena4/{par}")
	@Produces({ "application/json" })
	public Collection<Razervacija> getMojeeee(@PathParam("par") String par,@Context HttpServletRequest req){
		rezDao = (RezervacijaDAO)ctx.getAttribute("rezDAO");

		ArrayList<Razervacija> adList = new ArrayList<Razervacija>();
		
		for (Razervacija k : rezDao.getRezervacije().values()) {
			adList.add(k);
				System.out.println("get .. "+k.getId());
			}
		
		Comparator<Razervacija>compare=(Razervacija o1 ,Razervacija o2)->Double.compare(o1.getCijena(),o2.getCijena());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return adList;	
	}

	@GET
	@Path("/cijena/{par}")
	@Produces({ "application/json" })
	public Collection<Razervacija> getMojeApp(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");//gost

		ArrayList<Razervacija> adList = new ArrayList<>();
		
		RezervacijaDAO apDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		if(user.getListaRezervacija() == null){
			return null;
		}
		
		for(Integer ad : user.getListaRezervacija()){
		        Razervacija r=apDao.find(ad);
		        adList.add(r);	
			}
		
		Comparator<Razervacija>compare=(Razervacija o1 ,Razervacija o2)->Double.compare(o1.getCijena(),o2.getCijena());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		return adList;	
	}
	
	
	
	
	@GET
	@Path("/getMojeRezervacijeKor")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Korisnik> getMojeApK(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");//gost

		ArrayList<Razervacija> adList = new ArrayList<>();
		ArrayList<Korisnik> korisnici = new ArrayList<>();

		
		RezervacijaDAO rezDao = (RezervacijaDAO) this.ctx.getAttribute("rezDAO");
		
		UserDAO korDao = (UserDAO) ctx.getAttribute("korDAO");

		
//		if(user.getListaRezervacija() == null){
//			return null;
//		}
//		
//		for(Integer ad : user.getListaRezervacija()){
//		        Razervacija r=apDao.find(ad);
//		        adList.add(r);
//					
//				
//			}
//		for(Razervacija k :adList) {
//			Korisnik gost=k.getGost();
//			korisnici.add(gost);
//		}
		
		
		for(String s:user.getApartmaniZaIzdavanje()) {
			for(Razervacija r:rezDao.getRezervacije().values()) {
				if(r.getApartman().getId().equals(s)) {
					korisnici.add(r.getGost());
				}
			}
		}
		
		
		
		return korisnici;	
	}

	
	
	
	
	
	
	
	
	
	
	
	
	

}
