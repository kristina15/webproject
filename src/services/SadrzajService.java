
package services;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Korisnik;
import beans.Razervacija;
import beans.Sadrzaj;
import dao.SadrzajDAO;

@Path("/sadrzaj")

public class SadrzajService {
	@Context
	ServletContext ctx;
	private SadrzajDAO sadDao = new SadrzajDAO();

	public SadrzajService() {
		
	}
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("sadDAO") == null) {
			String contextPath = ctx.getRealPath("");
	    	sadDao = new SadrzajDAO(contextPath);
	    	System.out.println(contextPath+" PUTANJA-INIT ");
			ctx.setAttribute("sadDAO",sadDao);
		}
	}
	
	@POST
	@Path("/dodajSadrzaj")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Sadrzaj dodajSadrzaj(Sadrzaj res) {
		
		

        
    	   sadDao = (SadrzajDAO) ctx.getAttribute("sadDAO");
		   String contextPath = ctx.getRealPath(""); 
		   
//		   boolean postoji = false;
//			for (Sadrzaj s : sadDao.getSadrzaj().values()) {
//				if (s.getId().equals(res.getId()))
//					postoji = true;
//		   
//		   
//			}
//		if(!postoji) {
		   res.setAktivan(true);
		   res.setAk("Da");
		   System.out.println("sadrzaj dodat");
		   sadDao.save(contextPath, res);
		   ctx.setAttribute("sadDAO", sadDao);
		   sadDao.saveSadrzaj();

		   System.out.println("dodaaaaaaaaaaaaaat");
//		}
//		else {
//			return null;
//		}
		   return res;
			
		
	}
	
	
	

	@PUT
	@Path("/izmjeni/{sadrzaj}")	
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response promjeni( @PathParam("sadrzaj") String sadrzaj,
			@Context HttpServletRequest request)throws NullPointerException {
		String contextPath = ctx.getRealPath(""); 
		SadrzajDAO sadDao = (SadrzajDAO) this.ctx.getAttribute("sadDAO");

		if (sadrzaj.contains("%20"))
			sadrzaj.replace("%20", " ");

		String[] parts = sadrzaj.split(",");
		String stariNaziv= parts[0];
		String naziv = parts[1];
  	   System.out.println("Sadrzaj za izmjenu je "+stariNaziv);	
		//Sadrzaj ret = (Sadrzaj) sadDao.getSadrzaj().get(stariNaziv);
		Sadrzaj k = sadDao.findByName(stariNaziv);
		System.out.println("kkkkkkkkkk" +k);
		
		sadDao.getSadrzaj().remove(stariNaziv);
		System.out.println("novi naziv "+naziv);
	
		k.setNaziv(naziv);
		
		//sadDao.getSadrzaj().put(k.getNaziv(),k);

		sadDao.saveSadrzaj();
		sadDao.save(contextPath, k);
		return Response.status(200).build();
		
	}
	
	@PUT
	@Path("/obrisi/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Sadrzaj delCat(@PathParam("name") String name, @Context HttpServletRequest req) throws NullPointerException{
		SadrzajDAO dao = (SadrzajDAO) this.ctx.getAttribute("sadDAO");
		
		Sadrzaj k = dao.findByName(name);
		System.out.println(name);
		if(k!=null) {
		 k.setAk("Ne");
		 k.setAktivan(false);
         System.out.println("obrisano");
         //System.out.println(k.getId());
         
		}else {
			System.out.println("ne brise");
			//System.out.println(k.getId());
			
		}
	
		//ctx.setAttribute("sadDAO", sadDAO);
		dao.saveSadrzaj();
		return k;
		//return Response.status(200).build();
		
	}
	
	@PUT
	@Path("/activate/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activateAcc(@PathParam("id") String id, @Context HttpServletRequest request) {
		SadrzajDAO dao = (SadrzajDAO) this.ctx.getAttribute("sadDAO");	 
	    Sadrzaj k = dao.findByName(id);
		k.setAk("Da");
		k.setAktivan(true);
		
		//   ctx.setAttribute("sadDAO", sadDAO);
		dao.saveSadrzaj();
		
		return Response.status(200).build();
	}
	

	
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Sadrzaj> getSadrzaj(@QueryParam("username") String username) {
		sadDao = (SadrzajDAO)ctx.getAttribute("sadDAO");
	if(username!=null) {
		ArrayList<Sadrzaj> pretrazeni = new ArrayList<Sadrzaj>();
		for (Sadrzaj k : sadDao.getSadrzaj().values()) {
			if(k.getId().contains(username)) {
				pretrazeni.add(k);
			}
		}
		return pretrazeni;
		
	}else {
	
		ArrayList<Sadrzaj> sad = new ArrayList<Sadrzaj>();
		
		for (Sadrzaj k : sadDao.getSadrzaj().values()) {
				sad.add(k);
			}
		
		return sad;
	}
}
}



















