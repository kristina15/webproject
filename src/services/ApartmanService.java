package services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import beans.Apartman;
import beans.Korisnik;
import beans.Sadrzaj;
import dao.ApartmanDAO;
import dao.SadrzajDAO;
import dao.UserDAO;


@Path("/apartman")

public class ApartmanService {
	
	@Context
	ServletContext ctx;
	private ApartmanDAO apDao = new ApartmanDAO();

//	public ApartmanService() 
//		
//	}
	@PostConstruct
	public void init() {
		if (ctx.getAttribute("apDAO") == null) {
			String contextPath = ctx.getRealPath("");
			apDao = new ApartmanDAO(contextPath);
	    	System.out.println(contextPath+"   apartman ");
			ctx.setAttribute("apDAO",apDao);
		}
	}
	
	
	
	@POST
	@Path("/izmjena")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Apartman promijeni(Apartman user,@Context HttpServletRequest request)throws NullPointerException {
		apDao= (ApartmanDAO) this.ctx.getAttribute("apDAO");
		String contextPath = ctx.getRealPath(""); 	
		
		ArrayList<Apartman> users=getApartmane();
		
		System.out.println("*******************");
		Apartman loggedUser=apDao.find(user.getId());
		System.out.println("Ap za izmjenu");
		System.out.println(loggedUser);
		System.out.println("ap za izmjenu");
		for(Apartman u:users) {
			if(loggedUser!=null && u.getId().equals(user.getId())) {
				//&& u.getId().equals(user.getId())
				String tip=user.getTip();
				u.setTip(tip);
				String lokacija=user.getLokacija();
				u.setLokacija(lokacija);
				int brojSoba =user.getBrojSoba();
				u.setBrojSoba(brojSoba);
				int brojGostiju =user.getBrojGostiju();
				u.setBrojGostiju(brojGostiju);
				
				String komentari=user.getKomentari();
				//System.out.println(komentari);
				u.setKomentari(komentari);
				String datumOd=user.getDatumOd();
				u.setDatumOd(datumOd);
				String datumDo=user.getDatumDo();
				u.setDatumDo(datumDo);
				String status=user.getStatus();
				u.setStatus(status);
				
			}
		}
		System.out.println(loggedUser);
		apDao.delete(contextPath, loggedUser);
		//request.getSession().setAttribute("izmijeniAp",user);
		users.add(user);
		System.out.println(user.getCijenaPoNoci());
		ctx.setAttribute("apDAO", apDao);
		
		apDao.save(contextPath, user);
		
		apDao.saveApartman();
		return user;
	}
	
	@POST
	@Path("/dodajApartman")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Apartman dodajApartman(Apartman res) throws IOException {
		apDao = (ApartmanDAO) ctx.getAttribute("apDAO");
        UserDAO userDao = (UserDAO) ctx.getAttribute("korDAO");

		String contextPath = ctx.getRealPath(""); 
//		    String base64Image = (res.getImgSlika()).split(",")[1];
//			byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
//
//			BufferedImage img = ImageIO.read(new ByteArrayInputStream(imageBytes));
//			
//			File outputfile = new File(contextPath+"/slike/"+res.getId()+".jpg");
//			ImageIO.write(img, "jpg", outputfile);
//			res.setImgSlika("slike/"+res.getId()+".jpg");
		   
			if(apDao.getApartmani().values().contains(res)) {
				System.out.println("Apartman vec postoji");
				return null;
			}

			Korisnik k=userDao.findUserByUsername(res.getDomacin());
			ArrayList<String> apZaIzdavanje=k.getApartmaniZaIzdavanje();
			apZaIzdavanje.add(res.getId());
			
			k.setApartmaniZaIzdavanje(apZaIzdavanje);
			userDao.saveUsers();
			apDao.saveApartman();
			apDao.save(contextPath, res);
		   ctx.setAttribute("apDAO", apDao);
		   System.out.println("apartman dodat");
		   
		   System.out.println("okkkkk"); 
		   return  res;
	}

	
	
	@GET
	@Path("/getMojeApartmaneN")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Apartman> getMojeAp(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani() == null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("NEAKTIVAN")) {
				adList.add(ad);
				System.out.println("moj oglas");
				
					
				}
			}
		}}
		
		return adList;	
	}

	@GET
	@Produces({ "application/json" })
	public ArrayList<Apartman> getApartmane() {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");

		ArrayList<Apartman> ap = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
				ap.add(k);
				System.out.println("get .. "+k.getId());
			}
		
		
		return ap;
	
	}
	
	@GET
	@Path("/cijena2/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojeeee(@PathParam("par") String par,@Context HttpServletRequest req){
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
			adList.add(k);
				System.out.println("get .. "+k.getId());
			}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return adList;	
	}
	

	
	@GET
	@Path("/ap")
	@Produces({ "application/json" })
	public ArrayList<Apartman> getApartmanee() throws NoSuchAlgorithmException {
		
		
		
		try {
		ArrayList<Apartman> users=getApartmane();
		return users;
		
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return null;
	
	}
	
	
	@PUT
	@Path("/obrisi/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delCat(@PathParam("name") String name, @Context HttpServletRequest req) throws NullPointerException{
		if (name.contains("%20"))
    		name.replace("%20", " ");
		ApartmanDAO dao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
		
		Apartman k = dao.find(name);
		System.out.println("Brisanje apartmana.Id apartmana koji brisemo:");
		System.out.println(name);
		if(k!=null) {
		 k.setStatus("NEAKTIVAN");
		 
         System.out.println("obrisano");
         
		}else {
			System.out.println("ne brise");
			
		}
	
		//ctx.setAttribute("sadDAO", sadDAO);
		dao.saveApartman();
		//return k;
		return Response.status(200).build();
		
	}

	

	@PUT
	@Path("/activate/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response activateAcc(@PathParam("id") String id, @Context HttpServletRequest request) {
		if (id.contains("%20"))
    		id.replace("%20", " ");
		ApartmanDAO dao = (ApartmanDAO) this.ctx.getAttribute("apDAO");	 
	    Apartman k = dao.find(id);
		k.setStatus("AKTIVAN");
		//   ctx.setAttribute("sadDAO", sadDAO);
		dao.saveApartman();
		
		return Response.status(200).build();
	}
	

	
	
	
	
	
//	@PUT
//	@Path("/obrisi/{name}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response delCat(@PathParam("name") String name, @Context HttpServletRequest req) throws NullPointerException{
//		if (name.contains("%20"))
//			name.replace("%20", " ");
//		ApartmanDAO dao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
//		
//		Apartman k = dao.find(name);
//		System.out.println("Brisanje apartmana.Id apartmana koji brisemo:");
//		System.out.println(name);
//		if(k!=null) {
//		 k.setStatus("NEAKTIVAN");
//         System.out.println("obrisano");
//         
//		}else {
//			System.out.println("ne brise");
//			
//		}
//	
//		//ctx.setAttribute("sadDAO", sadDAO);
//		dao.saveApartman();
//		//return k;
//		return Response.status(200).build();
//		
//	}
//
//	
//
//	@PUT
//	@Path("/activate/{id}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response activateAcc(@PathParam("id") String id, @Context HttpServletRequest request) throws NullPointerException{
//		if (id.contains("%20"))
//			id.replace("%20", " ");
//		ApartmanDAO dao = (ApartmanDAO) this.ctx.getAttribute("apDAO");	 
//	    Apartman k = dao.find(id);
//		k.setStatus("AKTIVAN");
//		dao.saveApartman();
//		
//		return Response.status(200).build();
//	}
	
	
	
	
	
	
	@GET
	@Path("/aktivan")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getAktivne(@QueryParam("username") String username) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
	if(username!=null) {
		ArrayList<Apartman> pretrazeni = new ArrayList<Apartman>();
		for (Apartman k : apDao.getApartmani().values()) {
			if(k.getStatus().equals("AKTIVAN")) {
				if(k.getId().contains(username)) {
					pretrazeni.add(k);
				}
			}
		}
		return pretrazeni;
		
	}else {
	
		
		ArrayList<Apartman> sad = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
			    if(k.getStatus().equals("AKTIVAN")) {
			    	sad.add(k);	
			    }
				
			}
		
		return sad;
	}
}
		
	
	

//	
	@GET
	@Path("/listaSadrzaja/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Sadrzaj> getSadrzaje(@PathParam("name") String name){
		SadrzajDAO sadDAO = (SadrzajDAO) ctx.getAttribute("sadDAO");
		ApartmanDAO apDAO = (ApartmanDAO) ctx.getAttribute("apDAO");
		Apartman apartman = apDAO.find(name);
		Collection<Sadrzaj> ads = new ArrayList<Sadrzaj>();
		for (String pom:apartman.getListaSadrzajaApartmana()) {
			Sadrzaj ad = sadDAO.find(pom);
			if ((ad!= null) && (ad.isAktivan()) && (ad.getAk().equals("Da"))) {
				ads.add(ad);
			}else{
				
			}
			
			//System.out.println(ad.getNaziv()+" dodat");
		}
          
		return ads;
	}

//	@GET//za aktivne apartmane izlistaj
//	@Path("/listaSadrzaja/{name}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Collection<Sadrzaj> getSadrzaje(@PathParam("name") String name){
//		SadrzajDAO sadDAO = (SadrzajDAO) ctx.getAttribute("sadDAO");
//		ApartmanDAO apDAO = (ApartmanDAO) ctx.getAttribute("apDAO");
//		Apartman apartman = apDAO.find(name);
//		Collection<Sadrzaj> ads = new ArrayList<Sadrzaj>();
//			
//			for (String pom:apartman.getListaSadrzajaApartmana()) {
//				System.out.println(pom);
//				if((pom!=null) && (pom!="")) {
//					Sadrzaj ad = sadDAO.find(pom);
//					if ((ad!= null) && (ad.isAktivan()) && (ad.getAk().equals("Da")))
//						ads.add(ad);
//					
//				}
//			}
//		
//		return ads;
//	}	

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> getApartmane(@QueryParam("username") String username) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
	if(username!=null) {
		ArrayList<Apartman> pretrazeni = new ArrayList<Apartman>();
		for (Apartman k : apDao.getApartmani().values()) {
			if(k.getId().contains(username)) {
				pretrazeni.add(k);
			}
		}
		return pretrazeni;
		
	}else {
	
		
		ArrayList<Apartman> sad = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
				sad.add(k);
			}
		
		return sad;
	}
}

	
	//, @QueryParam("cmin") int cmin,  @QueryParam("cmax") int cmax, @QueryParam("smin") int smin, @QueryParam("smax") int smax
	@GET
	@Path("/pretraziAp")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> Trjazi( @QueryParam("lokacija") String lokacija, @QueryParam("osoba") int osoba, @QueryParam("cmin") String cmin,  @QueryParam("cmax") String cmax, @QueryParam("smin") String smin, @QueryParam("smax") String smax) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
		
		ArrayList<Apartman> povratna = new ArrayList<Apartman>();
		ArrayList<Apartman> nova = new ArrayList<Apartman>();

		for(Apartman u :apDao.getApartmani().values()) {
			if(u.getStatus()=="AKTIVAN") {
			povratna.add(u);
			}
		}
		System.out.println("USAO U PRETRAGU ZA APARTMANE");
		boolean vrsiSeOsoba = false;
			if(osoba != 0 ) {
			
			System.out.println("usao u pratagu po br osoba "+ osoba);
			
			for(Apartman u : apDao.getApartmani().values()) {
				if(u.getBrojGostiju()==osoba==false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
				
						nova.add(u);
			        
						
					
				}
			}
		
			
		}
			vrsiSeOsoba = true;
	}
			boolean vrsiSeLokacija = false;
			
		if(lokacija != null && lokacija.equals("")==false && lokacija.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po lokaciji "+ lokacija);
			
			for(Apartman u : apDao.getApartmani().values()) {
				
				if(u.getLokacija().equalsIgnoreCase(lokacija) == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					
						
					
				}
			}
			 
			
		}
			vrsiSeLokacija = true;
		}
		
		
	boolean vrsiSeMinCijena = false;
		
		if(cmin != null && cmin.equals("")==false && cmin.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po minc "+ cmin);
			double pom = Double.parseDouble(cmin);
			for(Apartman o :apDao.getApartmani().values() ) {
				
				if(o.getCijenaPoNoci()<pom) {
					if(povratna.contains(o)) {
						povratna.remove(o);
						
					}
				}
			}
			 vrsiSeMinCijena = true;
			
		}
		
		boolean vrsiSeMaxCijena = false;
		
		if(cmax != null && cmax.equals("")==false && cmax.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po maxc "+ cmax);
			double pom = Double.parseDouble(cmax);
			for(Apartman o :apDao.getApartmani().values()) {
				
				if(o.getCijenaPoNoci()>pom) {
					if(povratna.contains(o)) {
						povratna.remove(o);
						
					}
				}
			}
			 vrsiSeMaxCijena = true;
			
		}
		
	boolean vrsiSeMinSoba = false;
		
		if(smin != null && smin.equals("")==false && smin.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po minc "+ smin);
			double pom = Double.parseDouble(smin);
			for(Apartman o :apDao.getApartmani().values() ) {
				
				if(o.getBrojGostiju()<pom) {
					if(povratna.contains(o)) {
						povratna.remove(o);
						
					}
				}
			}
			vrsiSeMinSoba = true;
			
		}
		
		boolean vrsiSeMaxxSoba = false;
		
		if(smin != null && smin.equals("")==false && smin.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po maxc "+ cmax);
			double pom = Double.parseDouble(smin);
			for(Apartman o :apDao.getApartmani().values()) {
				
				if(o.getBrojGostiju()>pom) {
					if(povratna.contains(o)) {
						povratna.remove(o);
						
					}
				}
			}
			vrsiSeMaxxSoba = true;
			
		}
			 
	
		
	
		if( vrsiSeOsoba == false && vrsiSeLokacija==false && vrsiSeMaxCijena==false && vrsiSeMinCijena==false && vrsiSeMaxxSoba==false && vrsiSeMinSoba==false ){
			return null;
			
		}
		
		for(Apartman u : povratna) {
			System.out.println("povr "+u.getId());
		}
		
		
		
	
			//return nova;
	
	
			return povratna;
	}
	
	
	
	
	
	
	
	
	
	
	
	@GET
	@Path("/pretraziApartmane")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Apartman> korisniciPretraga( @QueryParam("tip") String tip, @QueryParam("id") String id,  @QueryParam("brojS") String brojS, @QueryParam("brojG") String brojG, @QueryParam("lokacija") String lokacija, @QueryParam("domacin") String domacin, @QueryParam("kometar") String komentar, @QueryParam("cijena") double cijena, @QueryParam("status") String status) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
		
		ArrayList<Apartman> povratna = new ArrayList<Apartman>();
		for(Apartman u :apDao.getApartmani().values()) {
			povratna.add(u);
		}
		System.out.println("USAO U PRETRAGU ZA APARTMANE");
		boolean vrsiSeLokacija = false;
			if(lokacija != null  && lokacija.equals("")==false && lokacija.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po nazivu "+ lokacija);
			
			for(Apartman u : apDao.getApartmani().values()) {
				if(u.getLokacija().equalsIgnoreCase(lokacija) == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					}
				}
			}
			vrsiSeLokacija = true;
			
		}
			boolean vrsiSeTip = false;
			
		if(tip != null && tip.equals("")==false && tip.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po gradu "+ tip);
			
			for(Apartman u : apDao.getApartmani().values()) {
				
				if(u.getTip().equalsIgnoreCase(tip) == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					}
				}
			}
			 vrsiSeTip = true;
			
		}
		boolean vrsiSeBrojS = false;
		
       if(brojS != null && brojS.equals("")==false && brojS.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po broju sobe "+ brojS);
			
			for(Apartman u : apDao.getApartmani().values()) {
				int i=Integer.parseInt(brojS);
				
				if(u.getBrojSoba()==i == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					}
				}
			}
			 vrsiSeBrojS = true;
			
		}
		
       
		boolean vrsiSeBrojG = false;
		
	    if(brojG != null && brojG.equals("")==false && brojG.equals("undefined")==false) {
				
				System.out.println("usao u pratagu po broju gostiju "+ brojG);
				
				for(Apartman u : apDao.getApartmani().values()) {
					int i=Integer.parseInt(brojG);
					
					if(u.getBrojGostiju()==i == false) {
						if(povratna.contains(u)) {
							povratna.remove(u);
							
						}
					}
				}
				 vrsiSeBrojG = true;
				
			}
		
		boolean vrsiSeId = false;
		
	 if(id != null && id.equals("")==false && id.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ id);
		
		for(Apartman u : apDao.getApartmani().values()) {
			
			if(u.getId().equalsIgnoreCase(id) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSeId = true;
		
	}
		boolean vrsiSeDomacin = false;
		
	 if(domacin != null && domacin.equals("")==false && domacin.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ id);
		
		for(Apartman u : apDao.getApartmani().values()) {
			
			if(u.getDomacin().equalsIgnoreCase(domacin) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSeDomacin = true;
		
	}
	 
		boolean vrsiSeKomentar = false;
		
	 if(komentar != null && komentar.equals("")==false && komentar.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ id);
		
		for(Apartman u : apDao.getApartmani().values()) {
			
			if(u.getKomentari().equalsIgnoreCase(komentar) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		vrsiSeKomentar = true;
		
	}
	 
	 boolean vrsiSeCijena = false;
		
	 if(cijena != 0) {
		
		System.out.println("usao u pratagu po gradu "+ id);
		
		for(Apartman u : apDao.getApartmani().values()) {
			
			if(u.getCijenaPoNoci()==cijena == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		vrsiSeCijena = true;
		
	}
	 
	 boolean vrsiSeStatus = false;
		
	 if(status != null && status.equals("")==false && status.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ id);
		
		for(Apartman u : apDao.getApartmani().values()) {
			
			if(u.getStatus().equalsIgnoreCase(status) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		vrsiSeStatus = true;
		
	}

	 
	
		
	
		if( vrsiSeTip == false && vrsiSeId==false && vrsiSeLokacija==false && vrsiSeBrojG==false && vrsiSeBrojS==false&& vrsiSeStatus==false&& vrsiSeCijena==false&& vrsiSeKomentar==false && vrsiSeDomacin==false){//vrsiSeMaxxSoba
			return null;
			
		}
		
		for(Apartman u : povratna) {
			System.out.println("povr "+u.getId());
		}
		return povratna;
	}	
	
//	@GET
//	@Path("/brojSoba/{par}")
//	@Produces({ "application/json" })
//	public Collection <Apartman> sortPoBrojuSoba(@PathParam("par") String par, @Context HttpServletRequest request){
//		System.out.println("Sort po broju soba");
//		ApartmanDAO apDao = (ApartmanDAO) ctx.getAttribute("apDAO");
//		Collection <Apartman> ret=apDao.getSort(par);
//		return ret;
//	}

	@GET
	@Path("/cijena/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojeee(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani()== null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("AKTIVAN")) {
				adList.add(ad);
				}
			}
		}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return adList;	
	}
	
	
	@GET
	@Path("/brojSoba/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojeeA(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani()== null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("AKTIVAN")) {
				adList.add(ad);
				}
			}
		}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojSoba(),o2.getBrojSoba());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return adList;
		
	}	
	@GET
	@Path("/tip/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojTip(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani()== null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("AKTIVAN")) {
				adList.add(ad);
				
				}
			}
		}
		}
		
//		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
//		Collections.sort(adList,compare);
//				if(par.equals("asc")) {
//					Collections.sort(adList,compare);
//				}else if(par.equals("desc")) {
//					Collections.sort(adList,compare.reversed());
//				}		
//		System.out.println("usao u get sort");
		return adList;	
	}	

	@GET
	@Path("/brojGostiju/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojee(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani()== null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("AKTIVAN")) {
				adList.add(ad);
				}
			}
		}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojGostiju(),o2.getBrojGostiju());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return adList;
		
	}	

	
	
	
	
	@GET
	@Path("/getMojeApartmaneA")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Apartman> getMojeA(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO)this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani()== null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin()!=null) {
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				if(ad.getStatus().equals("AKTIVAN")) {
				adList.add(ad);
				System.out.println("moj app domacina aktivni");
				
					
				}
			}
		}}
		
		return adList;	
	}
	
	
	@GET
	@Path("/cijena1/{par}")
	@Produces({ "application/json" })
	public ArrayList<Apartman> getAktivnee(@QueryParam("username") String username,@PathParam("par") String par) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
	if(username!=null) {
		ArrayList<Apartman> pretrazeni = new ArrayList<Apartman>();
		for (Apartman k : apDao.getApartmani().values()) {
			if(k.getStatus().equals("AKTIVAN")) {
				if(k.getId().contains(username)) {
					pretrazeni.add(k);
				}
			}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
		Collections.sort(pretrazeni,compare);
				if(par.equals("asc")) {
					Collections.sort(pretrazeni,compare);
				}else if(par.equals("desc")) {
					Collections.sort(pretrazeni,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return pretrazeni;
		
	}else {
	
		
		ArrayList<Apartman> sad = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
			    if(k.getStatus().equals("AKTIVAN")) {
			    	sad.add(k);	
			    }
				
			}
		
		

		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
		Collections.sort(sad,compare);
				if(par.equals("asc")) {
					Collections.sort(sad,compare);
				}else if(par.equals("desc")) {
					Collections.sort(sad,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return sad;
	}
}
		
	@GET
	@Path("/brojSoba1/{par}")
	@Produces({ "application/json" })
	public ArrayList<Apartman> getAktivneee(@QueryParam("username") String username,@PathParam("par") String par) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
	if(username!=null) {
		ArrayList<Apartman> pretrazeni = new ArrayList<Apartman>();
		for (Apartman k : apDao.getApartmani().values()) {
			if(k.getStatus().equals("AKTIVAN")) {
				if(k.getId().contains(username)) {
					pretrazeni.add(k);
				}
			}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojSoba(),o2.getBrojSoba());
		Collections.sort(pretrazeni,compare);
				if(par.equals("asc")) {
					Collections.sort(pretrazeni,compare);
				}else if(par.equals("desc")) {
					Collections.sort(pretrazeni,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return pretrazeni;
		
	}else {
	
		
		ArrayList<Apartman> sad = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
			    if(k.getStatus().equals("AKTIVAN")) {
			    	sad.add(k);	
			    }
				
			}
		
		

		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojSoba(),o2.getBrojSoba());
		Collections.sort(sad,compare);
				if(par.equals("asc")) {
					Collections.sort(sad,compare);
				}else if(par.equals("desc")) {
					Collections.sort(sad,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return sad;
	}
}
	@GET
	@Path("/brojGostiju1/{par}")
	@Produces({ "application/json" })
	public ArrayList<Apartman> getAktivvnee(@QueryParam("username") String username,@PathParam("par") String par) {
		apDao = (ApartmanDAO)ctx.getAttribute("apDAO");
	if(username!=null) {
		ArrayList<Apartman> pretrazeni = new ArrayList<Apartman>();
		for (Apartman k : apDao.getApartmani().values()) {
			if(k.getStatus().equals("AKTIVAN")) {
				if(k.getId().contains(username)) {
					pretrazeni.add(k);
				}
			}
		}
		
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojGostiju(),o2.getBrojGostiju());
		Collections.sort(pretrazeni,compare);
				if(par.equals("asc")) {
					Collections.sort(pretrazeni,compare);
				}else if(par.equals("desc")) {
					Collections.sort(pretrazeni,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return pretrazeni;
		
	}else {
	
		
		ArrayList<Apartman> sad = new ArrayList<Apartman>();
		
		for (Apartman k : apDao.getApartmani().values()) {
			    if(k.getStatus().equals("AKTIVAN")) {
			    	sad.add(k);	
			    }
				
			}
		
		

		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojGostiju(),o2.getBrojGostiju());
		Collections.sort(sad,compare);
				if(par.equals("asc")) {
					Collections.sort(sad,compare);
				}else if(par.equals("desc")) {
					Collections.sort(sad,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return sad;
	}
}

	
	
	
	@GET
	@Path("/getMojeApartmanee")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Apartman> getMojeaA(@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani() == null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				adList.add(ad);
				System.out.println("moj app");
				
					
				
			}
		}
		
		return adList;

		
		//return nova;

	}
	
	
	
	
}


