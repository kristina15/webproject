package services;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.catalina.User;

import beans.Apartman;
import beans.Korisnik;
import beans.Logovanje;
import dao.ApartmanDAO;
import dao.UserDAO;

@Path("/users")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class UserService {

	@Context
	ServletContext ctx;
	private UserDAO korDao = new UserDAO();



	public UserService() {
		
	}
	@PostConstruct
	public void init() {
		if (this.ctx.getAttribute("korDAO") == null) {
			String contextPath = this.ctx.getRealPath("");
	    	korDao = new UserDAO(contextPath);
	    	System.out.println(contextPath+"ovo je putanja");
			this.ctx.setAttribute("korDAO", korDao);
		}
	}
	
	
	

	@GET
	@Produces({ "application/json" })
	public ArrayList<Korisnik> getKorisnike() {
		korDao = (UserDAO)ctx.getAttribute("korDAO");

		ArrayList<Korisnik> sviKor = new ArrayList<Korisnik>();
		
		for (Korisnik k : korDao.getUsers().values()) {
				sviKor.add(k);
				System.out.println("get kor "+k.getKorisnickoIme());
			}
		
		
		return sviKor;
	
	}
	














	@GET
	@Path("/reg")
	@Produces({ "application/json" })
	public Response getKorisnikee() throws NoSuchAlgorithmException {
		
		
		try {
		ArrayList<Korisnik> users=getKorisnike();	
		return Response.ok(users).build();
		
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return Response.ok().build();
	
	}


	
	
	
	
	@POST
	@Path("/registracijaG")
	@Produces({ "application/json" })
	public Korisnik registerUser(Korisnik user, @Context HttpServletRequest request) {
		UserDAO users = (UserDAO) this.ctx.getAttribute("korDAO");
		Korisnik ret = users.registerUser(user.getKorisnickoIme(), user.getLozinka(), user.getIme(), user.getPrezime(),
				user.getPol(),"Gost");
		if (ret == null)
			return null;
		request.getSession().setAttribute("ulogovan", ret);
		return ret;
	}
	
	
	@POST
	@Path("/registracijaD")
	@Produces({ "application/json" })
	public Korisnik registerUsedr(Korisnik user, @Context HttpServletRequest request) {
		UserDAO users = (UserDAO) this.ctx.getAttribute("korDAO");
		Korisnik ret = users.registerUser(user.getKorisnickoIme(), user.getLozinka(), user.getIme(), user.getPrezime(),
				user.getPol(),"Domacin");
		if (ret == null)
			return null;
		request.getSession().setAttribute("ulogovan", ret);
		return ret;
	}
	
	@POST
	@Path("/uloguj")
	@Produces({ "application/json" })

	public Korisnik login(Logovanje korisnik,@Context HttpServletRequest request) {
		korDao = (UserDAO) ctx.getAttribute("korDAO");
	
	System.out.println("DOSAO DA SE ULOGUJE");
	//System.out.println("Ime korisnika je " + korisnik.getUsername() + " sifra je "+korisnik.getPassword());
	if(korDao.find(korisnik.getKorisnicko(), korisnik.getLozinka()) != null) {
		
		System.out.println("Korisnik je " + korDao.find(korisnik.getKorisnicko(), korisnik.getLozinka()).getKorisnickoIme());
	
		request.getSession().setAttribute("ulogovan",korDao.find(korisnik.getKorisnicko(), korisnik.getLozinka()));
		return korDao.find(korisnik.getKorisnicko(), korisnik.getLozinka());
	}else {
		return null;
	}
	
	}
	
	@GET
	@Path("/izloguj")
	@Produces({ "application/json" })
	public Korisnik logout(@Context HttpServletRequest request) {
		Korisnik user = (Korisnik)request.getSession().getAttribute("ulogovan");		
		//Poni�tava sesiju i osloba�a objekte koji su bili vezani za nju
		System.out.println("IZLOGOVAN");
		request.getSession().invalidate();
	return user;
	}
	
	
	
	@POST
	@Path("/izmjena")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Korisnik promijeni(Korisnik user,@Context HttpServletRequest request) {
		
		korDao = (UserDAO)this. ctx.getAttribute("userDAO");
		String contextPath = ctx.getRealPath(""); 	
		ArrayList<Korisnik> users=getKorisnike();
		Korisnik loggedUser=null;
		System.out.println("*******************");
		loggedUser=(Korisnik)request.getSession().getAttribute("ulogovan");
		System.out.println("Korisnik za izmjenuuuuuuuuuu:");
		System.out.println(loggedUser);
		System.out.println(loggedUser.getIme());
		String korisnickoImea =user.getKorisnickoIme();

		for(Korisnik u:users) {
			if(loggedUser!=null && u.getKorisnickoIme().equalsIgnoreCase(loggedUser.getKorisnickoIme())) {
				System.out.println("log user uloga "+loggedUser.getUloga());
				System.out.println("uloga od usera je "+user.getUloga());
				String ime=user.getIme();
				u.setIme(ime);
				String prezime=user.getPrezime();
				u.setPrezime(prezime);
				String pol =user.getPol();
				u.setPol(pol);
				String korisnickoIme =user.getKorisnickoIme();
				u.setKorisnickoIme(korisnickoIme);
				String lozinka=user.getLozinka();
				u.setLozinka(lozinka);
				String uloga=user.getUloga();
				System.out.println("Uloga jeee"+uloga);
				u.setUloga(uloga);
				

			}
		}
		korDao.delete(contextPath, loggedUser);
		request.getSession().setAttribute("ulogovan",user);
		users.add(user);		
		System.out.println("izmijenjen korisnik");
		ctx.setAttribute("userDAO", korDao);
		korDao.save(contextPath, user);
		korDao.saveUsers();
		
		return user;
	}

	
	@GET
	@Path("/pretraziKorisnike")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Korisnik> korisniciPretraga(@QueryParam("naziv") String ime, @QueryParam("lozinka") String lozinka, @QueryParam("ime") String imeK, @QueryParam("prezime") String prezime,@QueryParam("pol") String pol,@QueryParam("uloga") String uloga) {
		korDao = (UserDAO)ctx.getAttribute("korDAO");
		
		ArrayList<Korisnik> povratna = new ArrayList<Korisnik>();
		for(Korisnik u :korDao.getUsers().values()) {
			povratna.add(u);
		}
		System.out.println("USAO U PRETRAGU");
		boolean vrsiSeIme = false;
			if(ime != null  && ime.equals("")==false && ime.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po nazivu "+ ime);
			
			for(Korisnik u : korDao.getUsers().values()) {
				if(u.getKorisnickoIme().equalsIgnoreCase(ime) == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					}
				}
			}
			vrsiSeIme = true;
			
		}
			boolean vrsiSeLozinka = false;
			
		if(lozinka != null && lozinka.equals("")==false && lozinka.equals("undefined")==false) {
			
			System.out.println("usao u pratagu po gradu "+ lozinka);
			
			for(Korisnik u : korDao.getUsers().values()) {
				
				if(u.getLozinka().equalsIgnoreCase(lozinka) == false) {
					if(povratna.contains(u)) {
						povratna.remove(u);
						
					}
				}
			}
			 vrsiSeLozinka = true;
			
		}
		
		
		boolean vrsiSeImeK = false;
		
	 if(imeK != null && imeK.equals("")==false && imeK.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ imeK);
		
		for(Korisnik u : korDao.getUsers().values()) {
			
			if(u.getIme().equalsIgnoreCase(imeK) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSeImeK = true;
		
	}
	 
		boolean vrsiSePrezime = false;
		
	 if(prezime != null && prezime.equals("")==false && prezime.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ prezime);
		
		for(Korisnik u : korDao.getUsers().values()) {
			
			if(u.getPrezime().equalsIgnoreCase(prezime) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSePrezime = true;
		
	}
	 
		boolean vrsiSeUloga = false;
		
	 if(uloga != null && uloga.equals("")==false && uloga.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ uloga);
		
		for(Korisnik u : korDao.getUsers().values()) {
			
			if(u.getUloga().equalsIgnoreCase(uloga) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSeUloga = true;
		
	}
	
	 
		boolean vrsiSePol = false;
		
	 if(pol != null && pol.equals("")==false && pol.equals("undefined")==false) {
		
		System.out.println("usao u pratagu po gradu "+ pol);
		
		for(Korisnik u : korDao.getUsers().values()) {
			
			if(u.getPol().equalsIgnoreCase(uloga) == false) {
				if(povratna.contains(u)) {
					povratna.remove(u);
					
				}
			}
		}
		 vrsiSePol = true;
		
	}
		
		
		
	
		if(vrsiSeIme == false && vrsiSeLozinka == false && vrsiSeImeK==false && vrsiSePol==false&& vrsiSeUloga==false&& vrsiSePrezime==false){
			return null;
			
		}
		
		for(Korisnik u : povratna) {
			System.out.println("povr "+u.getKorisnickoIme());
		}
		return povratna;
	}
	
	
	
	
	

	
	
	
	
	
	@GET
	@Path("/uzmiTrenutnog")
	@Produces({ "application/json" })
	public Korisnik logout(@QueryParam("o") String o,@Context HttpServletRequest request) {
		System.out.println("Usao u metodu uzmiApp ");

		UserDAO ap=(UserDAO) ctx.getAttribute("korDAO");
		Korisnik a=ap.findUserByUsername(o);
	    
		HashMap<String, Korisnik> mapa=ap.getUsers();
		Korisnik k=null;
		if(ap.getUsers().containsKey(o)) {
			k=ap.getUsers().get(o);
		}
		
		
		System.out.println("eeeeeeeeeeeeeeeeeee"+a);

		request.getSession().setAttribute("trenutni",a);

		System.out.println("Usao u metodu uzmiTrenutni id apartmana je "+ o);
		
		System.out.println("Usao u metodu d uzme trenutni");
		Korisnik api=(Korisnik)request.getSession().getAttribute("trenunti");

		System.out.println("Preuzeti metodi uzmiApp je "+k);
		return k;
	
	}

	
	
	@GET
	@Path("/cijena5/{par}")
	@Produces({ "application/json" })
	public Collection<Apartman> getMojeaaA(@PathParam("par") String par,@Context HttpServletRequest req){
		Korisnik user = (Korisnik) req.getSession(false).getAttribute("ulogovan");

		ArrayList<Apartman> adList = new ArrayList<Apartman>();
		
		ApartmanDAO apDao = (ApartmanDAO) this.ctx.getAttribute("apDAO");
		
		if(apDao.getApartmani() == null){
			return null;
		}
		
		for(Apartman ad : apDao.getApartmani().values()){
			if(ad.getDomacin().equals(user.getKorisnickoIme())){
				adList.add(ad);
				System.out.println("moj app");
				
					
				
			}
		}
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getCijenaPoNoci(),o2.getCijenaPoNoci());
		Collections.sort(adList,compare);
				if(par.equals("asc")) {
					Collections.sort(adList,compare);
				}else if(par.equals("desc")) {
					Collections.sort(adList,compare.reversed());
				}		
		System.out.println("usao u get sort");
		
		return adList;	
	}
	
	
	
	
	
	

	
	
}
