package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Korisnik;
import beans.Sadrzaj;


public class SadrzajDAO {
	private HashMap<String, Sadrzaj> sadrzaj = new HashMap<>();
	private String ctxpath;

	public SadrzajDAO() {

	}

	public SadrzajDAO(String contextPath) {
		this.ctxpath = contextPath;
		try {
			loadSadrzaj();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Sadrzaj find(String id) {
		if(sadrzaj.containsKey(id)) {
			Sadrzaj pronadjeni = sadrzaj.get(id);
				return pronadjeni;		
		}else {
			return null;	
			
		}
	}

	public Sadrzaj findByName(String name) {
         for(Sadrzaj s:sadrzaj.values()) {
        	 if(s.getNaziv().equals(name)) {
        		 return s;
        	 }
         }
         return null;

	}	

	public void save(String contextPath,Sadrzaj s) {
		if(sadrzaj.containsKey(s.getId())==false) {
		
		sadrzaj.put(s.getId(), s);
		System.out.println(contextPath+" stavlja jedan put. ");
		saveSadrzaj(); 
		}
	}
	
	

	public HashMap<String, Sadrzaj> getSadrzaj() {
		return this.sadrzaj;
	}

	public void setSadrzaj(HashMap<String, Sadrzaj> sadrazaj) {
		this.sadrzaj = sadrazaj;
	}
	
	
	
	
	public void saveSadrzaj(){
		ObjectMapper mapper = new ObjectMapper();

		ArrayList<Sadrzaj> ogl = new ArrayList<Sadrzaj>();
		for(Sadrzaj o : this.sadrzaj.values()){
				ogl.add(o);	
		}
				
		File oglFile = new File(this.ctxpath +"/content.json");	
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(oglFile, ogl);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
	
	
//	public void saveSadrzaj(){
//		ObjectMapper mapper = new ObjectMapper();
//		
//
//		
//		ArrayList<Sadrzaj> kat = new ArrayList<Sadrzaj>();
//		for(Sadrzaj temp : this.sadrzaj.values()){
//				kat.add(temp);	
//		}
//		
//		
//		File cutFile = new File(this.ctxpath +"/sadrzaji.json");
//		
//		try {
//			mapper.writerWithDefaultPrettyPrinter().writeValue(cutFile, kat);
//		} catch (JsonGenerationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (JsonMappingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//	

	private void loadSadrzaj() throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		

		StringBuilder json = new StringBuilder();
		String temp;

		
		//load customer
		File customerFile = new File(this.ctxpath +"/content.json");
		json.setLength(0);
		json = new StringBuilder();
		try{
			BufferedReader br  = new BufferedReader(new FileReader(customerFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Sadrzaj> kat = mapper.readValue(json.toString(), new TypeReference<ArrayList<Sadrzaj>>() {});
		
		for(Sadrzaj c: kat){
			
			this.sadrzaj.put(c.getNaziv(), c);
		}
		
	}
	
	
}
