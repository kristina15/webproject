package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import beans.Apartman;
import beans.Gost;
import beans.Korisnik;
import beans.Razervacija;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;

public class UserDAO {

	private HashMap<String, Korisnik> users = new HashMap<>();
	private String ctxpath;
	public HashMap<String, Korisnik> getUsers() {
		return users;
	}

	public UserDAO() {

	}
	

	public UserDAO(String contextPath) {
		this.ctxpath = contextPath;
		try{
			loadUsers();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public Korisnik registerUser(String username, String password, String ime, String prezime, String pol,String uloga) {
		//Korisnik user = new Korisnik(username, password, ime, prezime, pol, "Gost");
		
		Korisnik user = new Korisnik(username, password, ime, prezime, pol, uloga);
		boolean postoji = false;
		for (Korisnik korisnik : this.users.values()) {
			if (korisnik.getKorisnickoIme().equals(username))
				postoji = true;
		}
		if (!postoji) {
			this.users.put(user.getKorisnickoIme(), user);
//			user.setUloga("gost");
			saveUsers();
			return user;
		}
		return null;
	}
	
	
	public void save(String contextPath,Korisnik s) {
		if(users.containsKey(s.getKorisnickoIme())==false) {
		users.put(s.getKorisnickoIme(), s);
		System.out.println(contextPath+" stavlja jedan put--korsisnik. ");
		saveUsers(); 
		}
	}
	
	
	 public void delete(String contextPath,Korisnik s) {
			if(users.containsKey(s.getKorisnickoIme())==true) {
			
			users.remove(s.getKorisnickoIme(), s);
			System.out.println(contextPath+" stavlja jedan put--korsisnik. ");
			saveUsers(); 
			}
		}

	
    private void loadUsers() throws IOException {
		
        ObjectMapper mapper = new ObjectMapper();
		

		StringBuilder json = new StringBuilder();
		String temp;

		
		File oglFile = new File(this.ctxpath +"/users.json");
		json.setLength(0);
		json = new StringBuilder();
		try{
			BufferedReader br  = new BufferedReader(new FileReader(oglFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Korisnik> korisnici = mapper.readValue(json.toString(), new TypeReference<ArrayList<Korisnik>>() {});
		this.users.clear();
		for(Korisnik c: korisnici){
			
			this.users.put(c.getKorisnickoIme(), c);
		}
			
	}
    public void saveUsers(){
		ObjectMapper mapper = new ObjectMapper();

		ArrayList<Korisnik> ogl = new ArrayList<Korisnik>();
		for(Korisnik o : this.users.values()){
				ogl.add(o);	
		}
				
		File oglFile = new File(this.ctxpath +"/users.json");	
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(oglFile, ogl);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
	public Korisnik find(String username, String password) {
		System.out.println(" DOSAO U KLASU ZA FIND broj korisnika je "+users.size());
		if(users.containsKey(username)) {
			Korisnik pronadjeni = users.get(username);
			
			if(pronadjeni.getLozinka().equals(password)) {	
				return pronadjeni;
			}
			return pronadjeni;
			
		}
		return null;	
	}
	
	
	public Korisnik findUserByUsername(String username) {
		if(users.containsKey(username)) {
			Korisnik pronadjeni = users.get(username);
			return pronadjeni;
		}
		return null;
		
		
		
			
		
	}
	
	

}