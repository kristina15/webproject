package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Apartman;
import beans.Gost;
import beans.Korisnik;
import beans.Razervacija;
import beans.Sadrzaj;

public class RezervacijaDAO {

	
	private HashMap<Integer, Razervacija> sadrzaj = new HashMap<>();
	private String ctxpath;

	public RezervacijaDAO() {

	}

	public RezervacijaDAO(String contextPath) {
		this.ctxpath = contextPath;
		try {
			loadRezervacije();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Razervacija find(int id) {
		if(sadrzaj.containsKey(id)) {
			Razervacija pronadjeni = sadrzaj.get(id);
				return pronadjeni;		
		}else {
			return null;	
			
		}
	}
	
	//dodajRez(id1,rezervacija.getPocetak(),rezervacija.getBroj_Nocenja(),rezervacija.getApartman(),"KREIRANA",konCijena,rezervacija.getPoruka(),rezervacija.getStatus())
	
	public Razervacija dodajRez(int id1,Date pocetak,int brojNocenja, Apartman apartman,String status,double konCijena,String poruka,Korisnik gost){
		
		Razervacija r=new Razervacija(apartman,pocetak,brojNocenja,konCijena,poruka,gost,status,id1);
		this.sadrzaj.put(r.getId(), r);
		saveRezervacija();
		return r;
		
		
		
		/* Razervacija(Apartman apartman, Date pocetak, int broj_Nocenja, Double cijena, String poruka, Gost gost,
				String status, String id)
				*/
		
	}
	public void savee(Razervacija r) {
		save(ctxpath,r);
		
	}

	public void save(String contextPath,Razervacija s) {
		if(sadrzaj.containsKey(s.getId())==false) {
		
		sadrzaj.put(s.getId(), s);
		System.out.println(contextPath+" stavlja jedan put. ");
		saveRezervacija(); 
		}
	}
	
	

	public HashMap<Integer, Razervacija> getRezervacije() {
		return this.sadrzaj;
	}

	public void setSadrzaj(HashMap<Integer, Razervacija> sadrazaj) {
		this.sadrzaj = sadrazaj;
	}
	
	
	
	
	public void saveRezervacija(){
		ObjectMapper mapper = new ObjectMapper();

		ArrayList<Razervacija> ogl = new ArrayList<Razervacija>();
		for(Razervacija o : this.sadrzaj.values()){
				ogl.add(o);	
		}
				
		File oglFile = new File(this.ctxpath +"/rezervacije.json");	
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(oglFile, ogl);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Razervacija findById(int id) {
        for(Razervacija s:sadrzaj.values()) {
       	 if(s.getId()==id) {
       		 return s;
       	 }
        }
        return null;

	}	
	private void loadRezervacije() throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		

		StringBuilder json = new StringBuilder();
		String temp;

		
		//load customer
		File customerFile = new File(this.ctxpath +"/rezervacije.json");
		json.setLength(0);
		json = new StringBuilder();
		try{
			BufferedReader br  = new BufferedReader(new FileReader(customerFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Razervacija> kat = mapper.readValue(json.toString(), new TypeReference<ArrayList<Razervacija>>() {});
		
		for(Razervacija c: kat){
			
			this.sadrzaj.put(c.getId(), c);//kljuc mape je id koji sam dodala
		}
		
	}
	
	
	
	
	
}
