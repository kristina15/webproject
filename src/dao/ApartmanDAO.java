package dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import beans.Apartman;
import beans.Razervacija;

public class ApartmanDAO {
	private HashMap<String, Apartman> apartman = new HashMap<>();
	private String ctxpath;

	public ApartmanDAO() {

	}

	public ApartmanDAO(String contextPath) {
		this.ctxpath = contextPath;
		try {
			loadApartman();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Apartman find(String id) {
		if(apartman.containsKey(id)) {
			Apartman pronadjeni = apartman.get(id);
				return pronadjeni;		
		}
		return null;	
	}
	


	public void save(String contextPath,Apartman s) {
		if(apartman.containsKey(s.getId())==false) {
		
		apartman.put(s.getId(), s);
		System.out.println(contextPath+"apartman stavlja jedan put. ");
		saveApartman(); 
		}
	}


	 public void delete(String contextPath,Apartman s)throws NullPointerException {
		 System.out.println(s.getId());
			if(apartman.containsKey(s.getId())==true) {
				
				apartman.remove(s.getId(), s);
				System.out.println(contextPath+" stavlja jedan put--appppp");
				saveApartman(); 
			}else {
				System.out.println("ne radi ovo nista");
			}
			
		}	
	

	public HashMap<String, Apartman> getApartmani() {
		return apartman;
	}

	public void setApartman(HashMap<String, Apartman> apartman) {
		this.apartman = apartman;
	}
	
	public void saveApartman(){
		ObjectMapper mapper = new ObjectMapper();
		

		
		ArrayList<Apartman> kat = new ArrayList<Apartman>();
		for(Apartman temp : this.apartman.values()){
				kat.add(temp);	
		}
		
		
		File cutFile = new File(this.ctxpath +"/apartmani.json");
		
		try {
			mapper.writerWithDefaultPrettyPrinter().writeValue(cutFile, kat);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	private void loadApartman() throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		

		StringBuilder json = new StringBuilder();
		String temp;

		
		//load customer
		File customerFile = new File(this.ctxpath +"/apartmani.json");
		json.setLength(0);
		json = new StringBuilder();
		try{
			BufferedReader br  = new BufferedReader(new FileReader(customerFile));
			while((temp = br.readLine()) != null){
				json.append(temp);
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		List<Apartman> kat = mapper.readValue(json.toString(), new TypeReference<ArrayList<Apartman>>() {});
		
		for(Apartman c: kat){
			
			this.apartman.put(c.getId(), c);
		}
		
	}
	private boolean postoji=false;
	public void brisanjeDatuma(Apartman ap,ArrayList<String>datumi) throws ParseException {
		ArrayList<String> dostupni=new ArrayList<>();
		for(String d:ap.getDostupniDatumi()) {
			for(String s:datumi) {
				if(s.equals(d)) {
			    	postoji=true;
				}
				
				
			}
			
			
			if(!postoji) {
				dostupni.add(d);
				postoji=false;
			}else {
				postoji=false;
				System.out.println("nista");;
			}
		}
		
		Apartman a=find(ap.getId());
		a.setDostupniDatumi(dostupni);
		saveApartman();
        
		System.out.println("koncna obrisana lista je ");
		for(String s:ap.getDostupniDatumi()) {
			System.out.println(s);
		}
	}


	public ArrayList<Apartman> uzmiListu(){
		ArrayList<Apartman> ap=new ArrayList<>();
		for(Apartman a:apartman.values()) {
			ap.add(a);
		}
		return ap;
	}
	
	
	public Collection<Apartman> getSort(String par){
		ArrayList<Apartman> sortt=new ArrayList<Apartman>(apartman.values());
		Comparator<Apartman>compare=(Apartman o1 ,Apartman o2)->Double.compare(o1.getBrojGostiju(),o2.getBrojGostiju());
		Collections.sort(sortt,compare);
				if(par.equals("asc")) {
					Collections.sort(sortt,compare);
				}else if(par.equals("desc")) {
					Collections.sort(sortt,compare.reversed());
				}		
		System.out.println("usao u get sort");
		return sortt;

	}
	
	

}
